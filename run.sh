# bin/bash

graph_files=("datasets/medium/mediumGraph.txt" "datasets/large/large.txt" "datasets/large/large.txt")
workload_files=("datasets/medium/mediumWorkload_static_FINAL.txt" "datasets/large/largeWorkload_48000_40.txt" "datasets/large/largeWorkload_6000_20.txt")
result_files=("datasets/medium/mediumWorkload_static_RESULTS.txt" "datasets/large/largeWorkload_48000_40_RESULTS.txt" "datasets/large/largeWorkload_6000_20_RESULTS.txt")

for ((i = 0; i <= 2; i++))
do
    echo "--------------------------------------"
    echo "Running ${workload_files[$i]}"
    #time bin/graph -g ${graph_files[${i}]} -w ${workload_files[${i}]} 1> /dev/null
    time bin/graph -g ${graph_files[${i}]} -w ${workload_files[${i}]} 1> output
    diff output ${result_files[${i}]}    
done
rm output
