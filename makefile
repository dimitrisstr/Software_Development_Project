CC = gcc
OUT = graph
BIN_DIR = bin
SRC_DIR = src
OBJS = main.o queue.o node_index.o grail_index.o hash.o buffer.o scc.o stack.o cc_index.o update_index.o read.o job.o job_scheduler.o bfs.o malloc.o
OBJS_DIR = $(addprefix $(BIN_DIR)/,$(OBJS))
CFLAGS = -c -o $@ -Wall -O3
LFLAGS = -pthread

all: $(OBJS_DIR)
	$(CC) -o $(BIN_DIR)/$(OUT) $(OBJS_DIR) $(LFLAGS)
	
$(OBJS_DIR): | $(BIN_DIR)

$(BIN_DIR)/main.o: $(SRC_DIR)/main.c
	$(CC) $(CFLAGS) $<

$(BIN_DIR)/node_index.o: $(SRC_DIR)/indexes/node_index.c $(SRC_DIR)/indexes/node_index.h
	$(CC) $(CFLAGS) $<
	
$(BIN_DIR)/buffer.o: $(SRC_DIR)/indexes/buffer/buffer.c $(SRC_DIR)/indexes/buffer/buffer.h
	$(CC) $(CFLAGS) $<

$(BIN_DIR)/bfs.o: $(SRC_DIR)/bfs/bfs.c
	$(CC) $(CFLAGS) $<
	
$(BIN_DIR)/scc.o: $(SRC_DIR)/indexes/static/scc.c
	$(CC) $(CFLAGS) $<

$(BIN_DIR)/malloc.o: $(SRC_DIR)/malloc/malloc.c
	$(CC) $(CFLAGS) $<
	
$(BIN_DIR)/read.o: $(SRC_DIR)/read/read.c $(SRC_DIR)/read/read.h
	$(CC) $(CFLAGS) $<
	
$(BIN_DIR)/job.o: $(SRC_DIR)/job_structure/job.c
	$(CC) $(CFLAGS) $<
	
$(BIN_DIR)/job_scheduler.o: $(SRC_DIR)/job_structure/job_scheduler.c $(SRC_DIR)/job_structure/job_scheduler.h
	$(CC) $(CFLAGS) $<
	
$(BIN_DIR)/hash.o: $(SRC_DIR)/data_structures/hash.c $(SRC_DIR)/data_structures/hash.h
	$(CC) $(CFLAGS) $<
	
$(BIN_DIR)/queue.o: $(SRC_DIR)/data_structures/queue.c $(SRC_DIR)/data_structures/queue.h
	$(CC) $(CFLAGS) $<
	
$(BIN_DIR)/stack.o: $(SRC_DIR)/data_structures/stack.c $(SRC_DIR)/data_structures/stack.h
	$(CC) $(CFLAGS) $<
	
$(BIN_DIR)/grail_index.o: $(SRC_DIR)/indexes/static/grail_index.c
	$(CC) $(CFLAGS) $<
	
$(BIN_DIR)/cc_index.o: $(SRC_DIR)/indexes/dynamic/cc_index.c
	$(CC) $(CFLAGS) $<
	
$(BIN_DIR)/update_index.o: $(SRC_DIR)/indexes/dynamic/update_index.c
	$(CC) $(CFLAGS) $<

$(BIN_DIR):
	mkdir -p $(BIN_DIR)
	
clean:	
	rm -f $(BIN_DIR)/$(OUT) $(OBJS_DIR)

