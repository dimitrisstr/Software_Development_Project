#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "indexes/node_index.h"
#include "read/read.h"

#define INDEX_SIZE 128


int main(int argc, char *argv[]) {
    /*
     * Program's arguments
     * -g graph_file
     * -w workload_file
     */
    int i;
    FILE *graph_file = NULL, *workload_file = NULL;

    for(i = 1; i < argc -1; i++) {
        if (strcmp(argv[i], "-g") == 0) {
            graph_file = fopen(argv[i + 1], "r");
            if (graph_file == NULL) {
                perror("graph file");
                if (workload_file != NULL)
                    fclose(workload_file);
                exit(EXIT_FAILURE);
            }
        }
        else if (strcmp(argv[i], "-w") == 0) {
            workload_file = fopen(argv[i + 1], "r");
            if (workload_file == NULL) {
                perror("workload file");
                if (graph_file != NULL)
                    fclose(graph_file);
                exit(EXIT_FAILURE);
            }
        }
    }

    if(graph_file == NULL || workload_file == NULL) {
        fprintf(stderr, "usage: ./graph -g graph_file -w workload_file\n");
        exit(EXIT_FAILURE);
    }

    NodeIndexPtr index = createNodeIndex(INDEX_SIZE);
    //create graph from file
    readGraph(graph_file, index);
    fclose(graph_file);
    //execute queries and additions
    readCommands(workload_file, index);
    fclose(workload_file);

    destroyNodeIndex(&index);
    exit(EXIT_SUCCESS);
}



