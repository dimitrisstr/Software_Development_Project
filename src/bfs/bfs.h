#ifndef _DYNAMIC_BFS_H_
#define _DYNAMIC_BFS_H_

#include <stdint.h>
#include "../indexes/node_index.h"
#include "../indexes/dynamic/cc_index.h"
#include "../data_structures/queue.h"
#include "../indexes/static/grail_index.h"
#include "../indexes/static/scc.h"

#define NOT_FOUND 0
#define FOUND 1

typedef enum Direction {
    backwards = -1,
    forward = 1
} Direction;

int estimateShortestPathStronglyConnectedComponents(SCCPtr components, NodeIndexPtr graph, uint32_t source,
                                                    uint32_t target, int *visited_array, int *bfs_id);

int sccStepBfs(Queue frontier, SCCPtr components, NodeIndexPtr graph, int *visited_array, int visited_value,
               uint32_t component_id, uint32_t target, ptr (*getListHead)(NodeIndexPtr, uint32_t, int *, int *),
               NodePtr (*getListNode)(NodeIndexPtr, Ptr));

int bidirectionalBFS(NodeIndexPtr index, CCPtr components, GrailIndexPtr grail_index, uint32_t source, uint32_t target,
                     uint32_t version, int *visited_array, int *bfs_id, JobFunction query_type);

int stepBFS(Queue frontier, NodeIndexPtr index, uint32_t target, uint32_t version, int *visited_array, int bfs_id,
            Direction bfs_direction, ptr (*getListHead)(NodeIndexPtr, uint32_t, int *, int *),
            NodePtr (*getListNode)(NodeIndexPtr, Ptr), GrailIndexPtr grail_index, JobFunction query_type);

#endif /* _DYNAMIC_BFS_H_ */


