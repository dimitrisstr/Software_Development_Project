#include "bfs.h"


int bidirectionalBFS(NodeIndexPtr index, CCPtr components, GrailIndexPtr grail_index, uint32_t source, uint32_t target,
                     uint32_t version, int *visited_array, int *bfs_id, JobFunction query_type) {
    if (source == target)
        return 0;

    if (query_type == job_dynamic_query)
        if (findNodeConnectedComponentID(components, source, version) != findNodeConnectedComponentID(components, target, version))
            return -1;

    Direction bfs_direction = forward;
    (*bfs_id)++;

    Queue out_queue, in_queue;
    Queue_create(&out_queue);
    Queue_create(&in_queue);
    Queue_push(out_queue, &source);
    Queue_push(in_queue, &target);

    uint32_t out_queue_size = 0, in_queue_size = 0;
    int step, found = 0, left = -1, right = -1;
    for (step = 0; found != FOUND && Queue_size(out_queue) != 0 && Queue_size(in_queue) != 0; step++) {
        out_queue_size = Queue_size(out_queue);
        in_queue_size = Queue_size(in_queue);
        if (out_queue_size < in_queue_size) {            // Step Forward
            bfs_direction = forward;
            left++;
            found = stepBFS(out_queue, index, target, version, visited_array, *bfs_id, bfs_direction,
                            &getListHeadOut, &NodeIndex_getListNodeOut, grail_index, query_type);
        }
        else {                                          // Step Backwards
            bfs_direction = backwards;
            right++;
            found = stepBFS(in_queue, index, source, version, visited_array, *bfs_id, bfs_direction,
                            &getListHeadIn, &NodeIndex_getListNodeIn, grail_index, query_type);
        }
    }

    Queue_destroy(&in_queue);
    Queue_destroy(&out_queue);
    int path_length = -1;

    if (found > 0) {
        if (left <= 0)
            path_length = right;
        else if (right <= 0)
            path_length = left;
        else
            path_length = left + right;
    }

    return path_length;
}


int stepBFS(Queue frontier, NodeIndexPtr index, uint32_t target, uint32_t version, int *bfs_visited_array,
            int bfs_id, Direction bfs_direction, ptr (*getListHead)(NodeIndexPtr, uint32_t, int *, int *),
            NodePtr (*getListNode)(NodeIndexPtr, Ptr), GrailIndexPtr grail_index, JobFunction query_type) {
    int i, last_node, last_record;
    uint32_t *node_id, *neighbors, *edge_property, queue_size = Queue_size(frontier);
    Ptr first_node;
    int opposite_version = -bfs_id, current_version = -opposite_version;

    if (bfs_direction == backwards) {
        current_version = -current_version;
        opposite_version = -opposite_version;
    }
    GRAIL_ANSWER answer;
    for (i = 0; i < queue_size; i++) {
        Queue_pop(frontier, (void **)&node_id);

        if (query_type == job_static_query) {
            if (bfs_direction == backwards)  //Step Backwards
                answer = isReachableGrailIndex(grail_index, target, *node_id);
            else                            //Step Forward
                answer = isReachableGrailIndex(grail_index, *node_id, target);
            if (answer == NO)
                continue;
        }

        // If first_node node_id has been visited by the opposite way, finish
        if (bfs_visited_array[*node_id] == opposite_version || *node_id == target)
            return FOUND;
        else if (bfs_visited_array[*node_id] != current_version) {
            bfs_visited_array[*node_id] = current_version;
            first_node = getListHead(index, *node_id, &last_record, &last_node);
            if (first_node == EMPTY)
                continue;

            int elements, current_ListNode = first_node, j;
            NodePtr current_node;

            while (current_ListNode != -1) {
                current_node = getListNode(index, current_ListNode);
                neighbors = getNeighbors(current_node);
                if (current_ListNode == last_node)
                    elements = last_record;
                else
                    elements = N;

                if (query_type == job_dynamic_query) {
                    edge_property = getEdgeProperties(current_node);
                    for (j = 0; j < elements; j++) {
                        if (edge_property[j] <= version && bfs_visited_array[neighbors[j]] != current_version)
                            Queue_push(frontier, &neighbors[j]);
                    }
                }
                else {
                    for (j = 0; j < elements; j++)
                        Queue_push(frontier, &neighbors[j]);
                }
                current_ListNode = getNextListNode(current_node);
            }
        }
    }
    return NOT_FOUND;
}


/*
 * SCC BFS
 */

int estimateShortestPathStronglyConnectedComponents(SCCPtr components, NodeIndexPtr graph, uint32_t source,
                                                    uint32_t target, int *visited_array, int *bfs_id) {
    uint32_t source_component = findNodeStronglyConnectedComponentID(components, source);
    (*bfs_id)++;
    int path_length = -1;

    if (source == target)
        return 0;

    Queue out_queue, in_queue;
    Queue_create(&out_queue);
    Queue_create(&in_queue);
    Queue_push(out_queue, &source);
    Queue_push(in_queue, &target);


    uint32_t *out_element, *in_element;
    int step, found = NOT_FOUND, start = -1, end = -1, out_queue_size, in_queue_size;

    for (step = 0; found != FOUND && Queue_size(out_queue) != 0 && Queue_size(in_queue) != 0; step++) {
        out_queue_size = Queue_size(out_queue);
        in_queue_size = Queue_size(in_queue);

        if (out_queue_size < in_queue_size) {                                       // Step Forward
            start++;
            found = sccStepBfs(out_queue, components, graph, visited_array, *bfs_id, source_component, target,
                               &getListHeadOut, &NodeIndex_getListNodeOut);
        }
        else if (out_queue_size > in_queue_size) {                                  // Step Backwards
            end++;
            found = sccStepBfs(in_queue, components, graph, visited_array, -(*bfs_id), source_component, source,
                               &getListHeadIn, &NodeIndex_getListNodeIn);
        }
        else {
            Queue_peek(out_queue, (void **)&out_element);
            Queue_peek(in_queue, (void **)&in_element);
            if (getSizeOut(graph, *out_element) < getSizeIn(graph, *in_element)) {    // Step Forward
                start++;
                found = sccStepBfs(out_queue, components, graph, visited_array, *bfs_id, source_component, target,
                                   &getListHeadOut, &NodeIndex_getListNodeOut);
            }
            else {                                                                 // Step Backwards
                end++;
                found = sccStepBfs(in_queue, components, graph, visited_array, -(*bfs_id), source_component, source,
                                   &getListHeadIn, &NodeIndex_getListNodeIn);
            }
        }
    }

    Queue_destroy(&in_queue);
    Queue_destroy(&out_queue);

    if (found > 0) {
        if (start < 0)
            path_length = end;
        else if (end < 0)
            path_length = start;
        else
            path_length = start + end;
    }

    return path_length;
}


int sccStepBfs(Queue frontier, SCCPtr components, NodeIndexPtr graph, int *visited_array, int visited_value,
               uint32_t component_id, uint32_t target, ptr (*getListHead)(NodeIndexPtr, uint32_t, int *, int *),
               NodePtr (*getListNode)(NodeIndexPtr, Ptr)) {
    int i, last_node, last_record, queue_size = Queue_size(frontier);
    uint32_t *node_id, *neighbors;
    Ptr first_node;

    for (i = 0; i < queue_size; i++) {
        Queue_pop(frontier, (void **)&node_id);
        // If first_node node_id has been visited by the opposite way, finish
        if (visited_array[*node_id] == -visited_value || *node_id == target)
            return FOUND;
        else if (visited_array[*node_id] != visited_value) {
            visited_array[*node_id] = visited_value;
            first_node = getListHead(graph, *node_id, &last_record, &last_node);
            if (first_node == EMPTY)
                continue;

            int elements, current_ListNode = first_node, j;
            NodePtr current_node;
            while (current_ListNode != -1) {
                current_node = getListNode(graph, current_ListNode);
                neighbors = getNeighbors(current_node);
                if (current_ListNode == last_node)
                    elements = last_record;
                else
                    elements = N;

                for (j = 0; j < elements; j++) {
                    if (findNodeStronglyConnectedComponentID(components, *node_id) == component_id)
                        Queue_push(frontier, &neighbors[j]);
                }
                current_ListNode = getNextListNode(current_node);
            }
        }
    }
    return NOT_FOUND;
}