#ifndef _QUEUE_H
#define _QUEUE_H

#include <stdint.h>
#include "../malloc/malloc.h"

#define QUEUENODE_SIZE 1000
#define SUCCESS 0
#define QUEUE_EMPTY 1

typedef struct QueueInfo* Queue;

int Queue_create(Queue *new_queue);

int Queue_push(Queue q, void *element);

int Queue_pop(Queue q, void **element);

int Queue_peek(Queue q, void **element);

uint32_t Queue_size(Queue q);

void Queue_destroy(Queue *q);

#endif /* _QUEUE_H */

