#ifndef _HASH_H_
#define _HASH_H_

#define SUCCESS 0
#define FAILED -1
#define BUCKET_SIZE 4

#include <stdlib.h>
#include <stdint.h>
#include "../malloc/malloc.h"

typedef struct HashTable* hashPtr;
typedef struct KeyList KeyList;

hashPtr HT_Construct(uint32_t);

int HT_Insert(hashPtr, uint32_t);

void HT_Delete(hashPtr, uint32_t);

int HT_Search(hashPtr, uint32_t);

void HT_Destroy(hashPtr*);

void HT_Print(hashPtr);

int HT_getb(hashPtr);

int HT_getSize(hashPtr);

#endif /* _HASH_H_ */
