#include "hash.h"

typedef struct Record {
	uint32_t key;
	uint32_t value;
} Record;

typedef struct Bucket {
	Record* keys;
	uint32_t last_record;
	uint32_t recs;
} Bucket;

typedef struct HashTable {
	Bucket* buckets;
	int keys, b;
} HashTable;


/*****************Bucket*****************/

void BU_Construct(Bucket* b) {
	b->keys = NULL;
	b->last_record = 0;
	b->recs = BUCKET_SIZE;
}

int BU_Insert(Bucket* b, uint32_t nodeId) {
	if (b->keys == NULL) {
		b->keys = c_malloc(BUCKET_SIZE * sizeof(Record));
	}
	if (b->last_record == b->recs) {
		b->recs = b->recs << 1;
		b->keys = c_realloc(b->keys, b->recs * sizeof(Record));
	}

	b->keys[b->last_record].key = nodeId;
	b->last_record++;
	return SUCCESS;
}


int BU_Search(Bucket* b, uint32_t key) {
	int i = 0;

	for (i = 0; i < b->last_record; i++) {
		if (b->keys[i].key == key) {
			return SUCCESS;
		}
	}

	return FAILED;
}

void BU_Destroy(Bucket* b) {
	free(b->keys);
}


/*****************Hash Table*****************/

int HT_getb(hashPtr h) {
	return h->b;
}

int HT_getSize(hashPtr h) {
	return h->keys;
}

hashPtr HT_Construct(uint32_t b) {
	int i = 0;
	hashPtr ht = c_malloc(sizeof(HashTable));

	ht->keys = 0;
	ht->b = b;
	ht->buckets = c_malloc(b * sizeof(Bucket));

	for (i = 0; i < b; i++) {
		BU_Construct(&(ht->buckets[i]));
	}

	return ht;
}

int HT_Insert(hashPtr h, uint32_t nodeId) {
	uint32_t i = nodeId % (h->b);

	h->keys++;
	return BU_Insert(&(h->buckets[i]), nodeId);
}


int HT_Search(hashPtr h, uint32_t nodeId) {
	uint32_t i = nodeId % (h->b);

	return BU_Search(&(h->buckets[i]), nodeId);
}

void HT_Destroy(hashPtr* h) {
	int i = 0;

	for (i = 0; i < (*h)->b; i++) {
		BU_Destroy(&((*h)->buckets[i]));
	}
	free((*h)->buckets);
	free(*h);
	*h = NULL;
}
