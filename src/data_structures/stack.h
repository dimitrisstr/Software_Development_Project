#ifndef _STACK_H
#define _STACK_H

#include <stdint.h>
#include "../malloc/malloc.h"

#define STACKNODE_SIZE 100

#define SUCCESS 0
#define STACK_EMPTY 1

typedef struct StackInfo* Stack;

int Stack_create(Stack *new_stack);

int Stack_push(Stack s, void *element);

int Stack_pop(Stack s, void **element);

int Stack_peek(Stack s, void **element);

uint32_t Stack_size(Stack s);

void Stack_destroy(Stack *s);

#endif /* _STACK_H */
