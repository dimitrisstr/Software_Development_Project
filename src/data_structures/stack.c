#include "stack.h"


typedef struct StackNode {
    void *values[STACKNODE_SIZE];
    struct StackNode *next;
} StackNode;


typedef struct StackInfo {
    uint32_t size;
    int top;
    struct StackNode *start;
} StackInfo;


int Stack_create(Stack *new_stack) {
    *new_stack = c_malloc(sizeof(StackInfo));

    (*new_stack)->size = 0;
    (*new_stack)->top = -1;
    (*new_stack)->start = c_malloc(sizeof(StackNode));
    (*new_stack)->start->next = NULL;

    return SUCCESS;
}


int Stack_push(Stack s, void *element) {
    s->top++;
    //if node is full, allocate a new node
    if (s->top == STACKNODE_SIZE) {
        StackNode *new_node;
        new_node = c_malloc(sizeof(StackNode));

        new_node->next = s->start;
        s->start = new_node;
        s->top = 0;
    }
    s->start->values[s->top] = element;
    s->size++;

    return SUCCESS;
}


int Stack_pop(Stack s, void **element) {
    if(Stack_size(s) != 0) {
        StackNode *temp;
        *element = s->start->values[s->top];
        s->size--;
        s->top--;
        //remove start node if node is empty
        if (s->top == -1 && Stack_size(s) != 0) {
            //set as start node the next node
            temp = s->start;
            s->start = s->start->next;
            free(temp);
            //set as top the last element of the next node
            s->top = STACKNODE_SIZE -1;
        }
        return SUCCESS;
    }
    else
        return STACK_EMPTY;
}


int Stack_peek(Stack s, void **element) {
    if (s->size != 0) {
        *element = s->start->values[s->top];
        return SUCCESS;
    }
    else {
        return STACK_EMPTY;
    }
}


uint32_t Stack_size(Stack s) {
    return s->size;
}


void Stack_destroy(Stack *s) {
    StackNode *temp1 = NULL, *temp2 = NULL;
    temp1 = (*s)->start;
    while (temp1 != NULL){
        temp2 = temp1;
        temp1 = temp1->next;
        free(temp2);
    }
    free(*s);
    *s = NULL;
}
