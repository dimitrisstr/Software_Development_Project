#include "queue.h"


typedef struct QueueNode {
    void *values[QUEUENODE_SIZE];
    struct QueueNode *next;
} QueueNode;


typedef struct QueueInfo {
    uint32_t size;
    uint32_t start_index;
    uint32_t last_index;
    QueueNode *start;
    QueueNode *last;
} QueueInfo;


int Queue_create(Queue *new_queue) {
    *new_queue = c_malloc(sizeof(QueueInfo));

    (*new_queue)->size = 0;
    (*new_queue)->start_index = 0;
    (*new_queue)->last_index = 0;
    (*new_queue)->start = NULL;
    (*new_queue)->last = NULL;
    return SUCCESS;
}


int Queue_push(Queue q, void *element) {
    QueueNode *new_node;
    if(q->start == NULL) {
        new_node = c_malloc(sizeof(QueueNode));

        new_node->next = NULL;
        q->start = new_node;
        q->last = new_node;
        q->last_index = 0;
    }
    else if (q->last_index == QUEUENODE_SIZE) {
        new_node = c_malloc(sizeof(QueueNode));

        q->last->next = new_node;
        q->last = new_node;
        new_node->next = NULL;
        q->last_index = 0;

    }
    q->last->values[q->last_index] = element;
    q->last_index++;
    q->size++;

    return SUCCESS;
}


int Queue_pop(Queue q, void **element) {
    if(q->size != 0) {
        QueueNode *temp;
        *element = q->start->values[q->start_index];
        q->size--;
        q->start_index++;
        //remove start node if node is empty
        if (q->start_index == QUEUENODE_SIZE) {
            //set as start node the next node
            temp = q->start;
            q->start = q->start->next;
            free(temp);
            //index of the first element
            q->start_index = 0;
        }
        return SUCCESS;
    }
    else
        return QUEUE_EMPTY;
}


int Queue_peek(Queue q, void **element) {
    if (q->size != 0) {
        *element = q->start->values[q->start_index];
        return SUCCESS;
    }
    else {
        return QUEUE_EMPTY;
    }
}


uint32_t Queue_size(Queue q) {
    return q->size;
}


void Queue_destroy(Queue *q) {
    QueueNode *temp1 = NULL, *temp2 = NULL;
    temp1 = (*q)->start;
    while (temp1 != NULL){
        temp2 = temp1;
        temp1 = temp1->next;
        free(temp2);
    }
    free(*q);
    *q = NULL;
}
