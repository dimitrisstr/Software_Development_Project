#include "read.h"


typedef enum GraphType {
    static_graph,
    dynamic_graph
} GraphType;


int readGraph(FILE *input_file, NodeIndexPtr index) {
    /*
     * input file format:
     * source_vertex destination_vertex     --Add edge from source_vertex
     *                                          to destination_vertex
     * S                                    --End of file
     */
    uint32_t start, end;
    char *start_token, *end_token, buffer[INPUT_BUFFER_SIZE +1];

    while(fgets(buffer, INPUT_BUFFER_SIZE, input_file) != NULL){
        start_token = strtok(buffer, " \t\n");
        end_token = strtok(NULL, " \t\n");

        if (strcmp(start_token, "S") == 0) {    //end of file
            break;
        }
        else {                                //add new edge
            start = atoi(start_token);
            end = atoi(end_token);
            insertNode(index, start, end, 0);
        }
    }

    return SUCCESS;
}


int readCommands(FILE *input_file, NodeIndexPtr index) {
    /*
     * input file format:
     * Q source_vertex destination_vertex   --Find minimum path from source_vertex
     *                                          to destination_vertex
     * A source_vertex destination_vertex   --Add edge from source_vertex
     *                                          to destination_vertex
     * F
     */
    GraphType graph_type;
    uint32_t source_node, target_node, version = 0, queries = 0;
    char *command_token, *start_token, *end_token, buffer[INPUT_BUFFER_SIZE +1];
    char previous_token = 'Q';
    double metric = 0.0;

    CCPtr weak_components = NULL;
    SCCPtr components = NULL;
    GrailIndexPtr grail_index = NULL;

    fgets(buffer, INPUT_BUFFER_SIZE, input_file);
    if (strcmp(buffer, "STATIC\n") == 0) {
        graph_type = static_graph;
        components = estimateStronglyConnectedComponents(index);
        grail_index = buildGrailIndex(index, components);
    }
    else if (strcmp(buffer, "DYNAMIC\n") == 0) {
        graph_type = dynamic_graph;
        weak_components = estimateConnectedComponents(index);
    }
    else {
        fprintf(stderr, "workload file: Wrong graph type expected STATIC or DYNAMIC\n");
        exit(EXIT_FAILURE);
    }

    JobSchedulerPtr scheduler = initializeScheduler(THREADS_NUM, grail_index, index, components, weak_components);
    JobPtr new_job;
    while(fgets(buffer, INPUT_BUFFER_SIZE, input_file) != NULL) {
        command_token = strtok(buffer, " \t\n");
        if(strcmp(command_token, "F") == 0) {
            executeAllJobs(scheduler);
            waitAllTasksToFinish(scheduler);
            printAllJobs(scheduler);
            resetScheduler(scheduler);
            if (graph_type == dynamic_graph) {
                completeIndexes(weak_components, version);
                if (queries != 0)
                    metric = getMetricVal(weak_components) / (double) queries;
                if (metric >= METRIC)
                    rebuildIndexes(weak_components, version);
            }
        }
        else {
            start_token = strtok(NULL, " \t\n");
            end_token = strtok(NULL, " \t\n");
            source_node = atoi(start_token);
            target_node = atoi(end_token);

            if (strcmp(command_token, "Q") == 0 && graph_type == static_graph) {
                new_job = createJob(0, source_node, target_node, job_static_query);
                submitJob(scheduler, new_job);
            }
            else if (strcmp(command_token, "Q") == 0 && graph_type == dynamic_graph) {
                new_job = createJob(version, source_node, target_node, job_dynamic_query);
                submitJob(scheduler, new_job);
                queries++;
                previous_token = 'Q';
            }
            else if (strcmp(command_token, "A") == 0) {
                if (previous_token == 'Q')
                    version++;
                insertNode(index, source_node, target_node, version);
                insertNewEdge(weak_components, source_node, target_node, version);
                previous_token = 'A';
            }
        }
    }

    destroyScheduler(&scheduler);
    if (graph_type == static_graph) {
        destroyGrailIndex(&grail_index);
        destroyStronglyConnectedComponents(&components);
    }
    else
        destroyConnectedComponents(weak_components);

    return SUCCESS;
}
