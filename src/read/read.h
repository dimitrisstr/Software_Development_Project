#ifndef _READ_H
#define _READ_H

#include <stdio.h>
#include <string.h>
#include "../malloc/malloc.h"
#include "../indexes/node_index.h"
#include "../indexes/static/grail_index.h"
#include "../indexes/dynamic/cc_index.h"
#include "../indexes/static/scc.h"
#include "../job_structure/job.h"
#include "../job_structure/job_scheduler.h"


#define SUCCESS 0
#define INPUT_BUFFER_SIZE 64
#define THREADS_NUM 8

int readGraph(FILE *input_file, NodeIndexPtr);

int readCommands(FILE *input_file, NodeIndexPtr);

#endif /* _READ_H */
