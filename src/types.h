#ifndef _TYPES_H
#define _TYPES_H

#define SUCCESS 0
#define FAILED -1

typedef int OK_SUCCESS;

typedef enum bool {false, true} bool;

typedef enum JobFunction {
    job_static_query,
    job_dynamic_query,
    job_exit
} JobFunction;

#endif /* _TYPES_H */
