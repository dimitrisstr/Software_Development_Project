#include "malloc.h"


void* c_malloc(size_t size) {
    void *m = malloc(size);
    if (m == NULL) {
        fprintf(stderr, "Error: malloc: Out of memory\n");
        exit(EXIT_FAILURE);
    }
    return m;
}


void* c_realloc(void *ptr, size_t size) {
    void *m = realloc(ptr, size);
    if (m == NULL) {
        fprintf(stderr, "Error: realloc: Out of memory\n");
        exit(EXIT_FAILURE);
    }
    return m;
}
