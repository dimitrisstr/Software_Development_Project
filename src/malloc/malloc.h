#ifndef _MALLOC_H
#define _MALLOC_H

#include <stdio.h>
#include <stdlib.h>

void* c_malloc(size_t size);

void* c_realloc(void *ptr, size_t size);

#endif /* _MALLOC_H */
