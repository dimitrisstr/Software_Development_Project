#ifndef _NODEINDEX_H_
#define _NODEINDEX_H_

#include <stdint.h>
#include "../malloc/malloc.h"
#include "buffer/buffer.h"
#include "../data_structures/hash.h"
#include "../types.h"

#define BUFFER_SIZE 128
#define HASH_SIZE 5
#define SUCCESS 0
#define FAILED -1
#define EDGE_EXISTS -2
#define EMPTY -3

typedef int ptr;

typedef struct NodeIndex* NodeIndexPtr;
typedef BufferIteratorPtr NodeIndexIteratorPtr;

NodeIndexPtr createNodeIndex(uint32_t size);

uint32_t getSize(NodeIndexPtr);

OK_SUCCESS insertNode(NodeIndexPtr, uint32_t nodeId, uint32_t targetId , uint32_t version);

OK_SUCCESS insertOutNode(NodeIndexPtr index, uint32_t nodeId, uint32_t targetId);

ptr getListHeadOut(NodeIndexPtr, uint32_t nodeId, int *last_record, int *last_node);

ptr getListHeadIn(NodeIndexPtr, uint32_t nodeId, int *last_record, int *last_node);

OK_SUCCESS destroyNodeIndex(NodeIndexPtr*);

uint32_t getSizeIn(NodeIndexPtr index, uint32_t nodeId);

uint32_t getSizeOut(NodeIndexPtr index, uint32_t nodeId);

NodePtr NodeIndex_getListNodeIn(NodeIndexPtr index, Ptr id);

NodePtr NodeIndex_getListNodeOut(NodeIndexPtr index, Ptr id);

NodeIndexIteratorPtr NodeIndex_initIteratorOut(NodeIndexPtr index, uint32_t node_id);

int NodeIndex_nextNeighbor(NodeIndexIteratorPtr *it, uint32_t *element);

#endif /* _NODEINDEX_H_ */
