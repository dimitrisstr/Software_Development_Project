#ifndef _CCINDEX_H_
#define _CCINDEX_H_

#include <limits.h>
#include "../../malloc/malloc.h"
#include "../node_index.h"
#include "update_index.h"
#include "../../data_structures/hash.h"
#include "../../data_structures/queue.h"

#define METRIC 0.03

typedef struct CC* CCPtr;

uint32_t getMetricVal(CCPtr);

uint32_t getComponentsCount(CCPtr);

CCPtr estimateConnectedComponents(NodeIndexPtr);

OK_SUCCESS insertNewEdge(CCPtr, uint32_t, uint32_t, uint32_t);

uint32_t findNodeConnectedComponentID(CCPtr, uint32_t, uint32_t);

OK_SUCCESS completeIndexes(CCPtr, uint32_t);

OK_SUCCESS rebuildIndexes(CCPtr, uint32_t);

OK_SUCCESS destroyConnectedComponents(CCPtr);

#endif /* _CCINDEX_H_ */
