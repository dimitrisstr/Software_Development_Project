#include "cc_index.h"


typedef struct CC {
	uint32_t *ccindex;
	UpdateIndexPtr update_index;
	uint32_t metricVal;
	uint32_t ncomponents;
	uint32_t nodes;
} CC;



void BFS(NodeIndexPtr graph, int start, int component, hashPtr visited, uint32_t *ccindex) {
	Queue frontier;
	int last_record, last_node, first_node, elements, current_ListNode, queue_size, i, j;
	uint32_t *node_id, *neighbors;
	NodePtr current_node;

	Queue_create(&frontier);
	Queue_push(frontier, &start);
	while ((queue_size = Queue_size(frontier)) != 0) {
		for (i = 0; i < queue_size; i++) {
			Queue_pop(frontier, (void **)&node_id);

			if (HT_Search(visited, *node_id) == FAILED) {
				// Mark the CC of expanding node
				ccindex[*node_id] = component;
				HT_Insert(visited, *node_id);

				// Get outgoing neighbor list start
				first_node = getListHeadOut(graph, *node_id, &last_record, &last_node);
				if (first_node != EMPTY) {
					// Iterate over neighbors
					current_ListNode = first_node;
					while (current_ListNode != -1) {
						current_node = NodeIndex_getListNodeOut(graph, current_ListNode);
						neighbors = getNeighbors(current_node);
						if (current_ListNode == last_node)
							elements = last_record;
						else
							elements = N;

						for (j = 0; j < elements; j++)
							Queue_push(frontier, &neighbors[j]);
						current_ListNode = getNextListNode(current_node);
					}
				}

				// Get incoming neighbor list start
				first_node = getListHeadIn(graph, *node_id, &last_record, &last_node);
				if (first_node == EMPTY)
					continue;
				current_ListNode = first_node;
				// Iterate over neighbors
				while (current_ListNode != -1) {
					current_node = NodeIndex_getListNodeIn(graph, current_ListNode);
					neighbors = getNeighbors(current_node);
					if (current_ListNode == last_node)
						elements = last_record;
					else
						elements = N;

					for (j = 0; j < elements; j++)
						Queue_push(frontier, &neighbors[j]);
					current_ListNode = getNextListNode(current_node);
				}
			}
		}
	}

	Queue_destroy(&frontier);
}


uint32_t maxNode(uint32_t a, uint32_t b) {
	return (a >= b ? a : b);
}

uint32_t getMetricVal(CCPtr components) {
	return components->metricVal;
}


uint32_t getComponentsCount(CCPtr components) {
	return components->ncomponents;
}


CCPtr estimateConnectedComponents(NodeIndexPtr graph) {
	CCPtr components = c_malloc(sizeof(CC));
	int i;

	components->nodes = getSize(graph);
	components->ccindex = c_malloc(components->nodes * sizeof(uint32_t));
	components->ncomponents = 0;
	components->metricVal = 0;
	hashPtr visited = HT_Construct(components->nodes/59 + 11);

	for (i = 0; i < components->nodes; i++) {
		if (HT_Search(visited, i) == FAILED) {
			BFS(graph, i, components->ncomponents, visited, components->ccindex);
			components->ncomponents++;
		}
	}

	components->update_index = makeSets(components->ncomponents);
	HT_Destroy(&visited);

	return components;
}


OK_SUCCESS insertNewEdge(CCPtr components, uint32_t nodeIdS, uint32_t nodeIdE, uint32_t version) {
	if ((nodeIdS >= components->nodes) || (nodeIdE >= components->nodes)) {
		int old_nodes = components->nodes, i;
		components->nodes = maxNode(nodeIdE, nodeIdS) + 1;
		components->ccindex = c_realloc(components->ccindex, components->nodes * sizeof(uint32_t));

		for (i = old_nodes; i < components->nodes; i++) {
			components->ccindex[i] = UINT_MAX;
		}
	}

	int flag = 0, flag1 = (components->ccindex[nodeIdS] > components->ncomponents), flag2 = (components->ccindex[nodeIdE] > components->ncomponents);

	flag = flag1 | flag2;
	// Check if there are new nodes
	if (flag1 || flag2) {
		// Determine which one is the new node and put it in the CC of the existing one (case 1 or 2)
		// If both nodes are new, create a new CC and assign it to both of them (case 3) 
		flag1 = flag1 << 1;
		flag = flag1 | flag2;
		switch (flag) {
			case 1: components->ccindex[nodeIdE] = find(components->update_index, components->ccindex[nodeIdS], version);
				return SUCCESS;

			case 2: components->ccindex[nodeIdS] = find(components->update_index, components->ccindex[nodeIdE], version);
				return SUCCESS;

			case 3: components->ccindex[nodeIdS] = components->ncomponents;
				components->ccindex[nodeIdE] = components->ncomponents;
				components->ncomponents++;
				rebuildSets(components->update_index, version);
				return SUCCESS;

			default: return FAILED;
		}
	}

	//  If no new nodes are added continue normally
	uint32_t componentS = components->ccindex[nodeIdS], componentE = components->ccindex[nodeIdE];
	// If the two nodes do not belong in the same CC connect the CCs
	if (find(components->update_index, componentS, version) != find(components->update_index, componentE, version))
		joinSets(components->update_index, componentS, componentE, version);

	return SUCCESS;
}


uint32_t findNodeConnectedComponentID(CCPtr components, uint32_t nodeId, uint32_t version) {
	uint32_t component = components->ccindex[nodeId];
	uint32_t new_component = find(components->update_index, component, version);

	if (component != new_component) {
		components->metricVal++;
	}

	return new_component;
}

OK_SUCCESS completeIndexes(CCPtr components, uint32_t version) {
	// Updates the index state after the end of a burst
	completeSets(components->update_index, version);

	return SUCCESS;
}


OK_SUCCESS rebuildIndexes(CCPtr components, uint32_t version) {
	int i;
	uint32_t max = components->ccindex[0];

	// Merge all components linked to one major CC
	for (i = 0; i < components->nodes; i++) {
		if (components->ccindex[i] >= 0 && components->ccindex[i] < components->ncomponents) {
			components->ccindex[i] = find(components->update_index, components->ccindex[i], version);
			if (components->ccindex[i] > max) {
				max = components->ccindex[i];
			}
		}
	}
	components->ncomponents = max;
	components->metricVal = 0;
	destroySets(&components->update_index);
	components->update_index = makeSets(max);

	return SUCCESS;
}


OK_SUCCESS destroyConnectedComponents(CCPtr components) {
	free(components->ccindex);
	destroySets(&components->update_index);
	free(components);

	return SUCCESS;
}
