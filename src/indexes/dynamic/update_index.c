#include "update_index.h"


typedef struct Element {
	uint32_t representative;
	int last;
	int next;
	uint32_t version;
} Element;

typedef struct UpdateIndex {
	uint32_t number_of_nodes;
	ElementPtr sets;
} UpdateIndex;



UpdateIndexPtr makeSets(uint32_t components) {
	UpdateIndexPtr ui = c_malloc(sizeof(UpdateIndex));
	uint32_t i;

	ui->number_of_nodes = components;
	ui->sets = c_malloc(components * sizeof(Element));

	for (i = 0; i < components; i++) {
		ui->sets[i].representative = i;
		ui->sets[i].last = i;
		ui->sets[i].next = -1;
		ui->sets[i].version = 0;
	}

	return ui;
}


void rebuildSets(UpdateIndexPtr ui, uint32_t version) {
	// Add a new connected component as a new list
	ui->number_of_nodes++;
	ui->sets = c_realloc(ui->sets, ui->number_of_nodes * sizeof(Element));
	ui->sets[ui->number_of_nodes - 1].representative = ui->number_of_nodes - 1;
	ui->sets[ui->number_of_nodes - 1].last = ui->number_of_nodes - 1;
	ui->sets[ui->number_of_nodes - 1].next = -1;
	ui->sets[ui->number_of_nodes - 1].version = version;
}

void completeSets(UpdateIndexPtr ui, uint32_t version) {
	int i = 0, j = 0;

	// Update all components so that they point to the current version's representative
	for (i = 0; i < ui->number_of_nodes; i++) {
		if (ui->sets[i].representative == i) {
			j = ui->sets[i].next;
			while (j != -1) {
				ui->sets[j].representative = i;
				ui->sets[j].version = version;
				if (ui->sets[j].next == -1)
					ui->sets[i].last = j;
				j = ui->sets[j].next;
			}
		}
	}
}


void joinSets(UpdateIndexPtr ui, uint32_t compIdS, uint32_t compIdE, uint32_t version) {
	// Connect the list with the greater representative to the end of the list with the lesser one
	uint32_t min = find(ui, compIdS, version);
	uint32_t max = find(ui, compIdE, version);
	uint32_t i, last;

	if (max < min) {
		i = min;
		min = max;
		max = i;
	}

	last = ui->sets[min].last;
	ui->sets[last].next = max;

	ui->sets[max].representative = min;
	ui->sets[max].version = version;
}


uint32_t find(UpdateIndexPtr ui, uint32_t compId, uint32_t version) {
	/*
	 * Search recursively for compId's representative
	 * until you exceed current version or find a terminal representative
	 */
	if (compId >= 0 && compId < ui->number_of_nodes) {
		uint32_t representative = ui->sets[compId].representative;
		uint32_t index = ui->sets[representative].representative;

		if (ui->sets[compId].version > version)
			return compId;
		while ((ui->sets[representative].version <= version) && (representative != index)) {
			representative = ui->sets[representative].representative;
			index = ui->sets[representative].representative;
		}
		return representative;
	}
	return compId;
}


void destroySets(UpdateIndexPtr* ui) {
	free((*ui)->sets);
	free(*ui);
	*ui = NULL;
}

