#ifndef _UPDATEINDEX_H_
#define _UPDATEINDEX_H_

#include <stdint.h>
#include "../../malloc/malloc.h"

typedef struct Element* ElementPtr;
typedef struct UpdateIndex* UpdateIndexPtr;

UpdateIndexPtr makeSets(uint32_t);

void rebuildSets(UpdateIndexPtr, uint32_t);

void completeSets(UpdateIndexPtr, uint32_t);

void joinSets(UpdateIndexPtr, uint32_t, uint32_t, uint32_t);

uint32_t find(UpdateIndexPtr, uint32_t, uint32_t);

void destroySets(UpdateIndexPtr*);

#endif /* _UPDATEINDEX_H_ */
