#include "node_index.h"


typedef struct IndexNode {
	uint32_t size_in, size_out;
	int index_in, index_out;
	int in_last_node, out_last_node;
	int in_last_record, out_last_record;
	hashPtr neighbors;
} IndexNode;


typedef struct NodeIndex {
	uint32_t size;
	int node_number;
	BufferPtr out_buffer;
	BufferPtr in_buffer;
	IndexNode* nodes;
} NodeIndex;


void resizeHash(NodeIndexPtr index, int nodeId);


uint32_t max(uint32_t a, uint32_t b) {
	return (a >= b ? a : b);
}


NodeIndexPtr createNodeIndex(uint32_t size) {
	int i = 0;
	NodeIndexPtr index = c_malloc(sizeof(NodeIndex));

	index->size = size;
	index->node_number = 0;
	index->out_buffer = createBuffer(BUFFER_SIZE);
	index->in_buffer = createBuffer(BUFFER_SIZE);
	index->nodes = c_malloc(size * sizeof(IndexNode));
	for (i = 0; i < size; i++) {
		index->nodes[i].index_in = EMPTY;
		index->nodes[i].index_out = EMPTY;
		index->nodes[i].in_last_node = EMPTY;
		index->nodes[i].in_last_record = 0;
		index->nodes[i].out_last_node = EMPTY;
		index->nodes[i].out_last_record = 0;
		index->nodes[i].size_out = 0;
		index->nodes[i].size_in = 0;
		index->nodes[i].neighbors = NULL;//HT_Construct(HASH_SIZE);
	}

	return index;
}


uint32_t getSize(NodeIndexPtr index) {
	return index->node_number;
}


void insertEdge(ptr *first_node, ptr *last_node, ptr *last_record,
				BufferPtr buffer, uint32_t target, uint32_t version) {
	NodePtr node = NULL;
	int new_ListNode;

	//If source node has no edges
	if (*first_node == EMPTY) {
		*first_node = allocNewNode(buffer);
		*last_node = *first_node;
	}

	node = getListNode(buffer, *last_node);
	if (addNeighbor(node, *last_record, target, version) == FAILED) {
		new_ListNode = allocNewNode(buffer);
		node = getListNode(buffer, *last_node);
		*last_node = new_ListNode;
		setNextListNode(node, new_ListNode);
		*last_record = 0;
		node = getListNode(buffer, *last_node);
		addNeighbor(node, *last_record, target, version);
	}
	(*last_record)++;
}


OK_SUCCESS insertOutNode(NodeIndexPtr index, uint32_t nodeId, uint32_t targetId) {
	/*
	 * Insert nodeId and outgoing edge
	 * (used to create hyper graph)
	 */

	uint32_t new_size = index->size;
	int i = 0;

	// If one node does not fit on table double the size until it fits
	while (nodeId >= new_size || targetId >= new_size) {
		new_size = new_size << 1;
	}
	if (new_size > index->size) {
		IndexNode* new_nodes = NULL;
		// Double the index's size
		new_nodes = c_realloc(index->nodes, new_size * sizeof(IndexNode));

		for (i = index->size; i < new_size; i++) {
			new_nodes[i].index_in = EMPTY;
			new_nodes[i].index_out = EMPTY;
			new_nodes[i].in_last_node = EMPTY;
			new_nodes[i].in_last_record = 0;
			new_nodes[i].out_last_node = EMPTY;
			new_nodes[i].out_last_record = 0;
			new_nodes[i].size_out = 0;
			new_nodes[i].size_in = 0;
			new_nodes[i].neighbors = NULL;//HT_Construct(HASH_SIZE);
		}
		index->size = new_size;
		index->nodes = new_nodes;
	}

	if (index->nodes[nodeId].neighbors == NULL) {
		index->nodes[nodeId].neighbors = HT_Construct(HASH_SIZE);
	}

	int max_node = max(nodeId, targetId);
	if( max_node > (index->node_number - 1))
		index->node_number = max_node + 1;

	if (HT_Search(index->nodes[nodeId].neighbors, targetId) != FAILED) {		
		return EDGE_EXISTS;
	}

	// Insert outgoing edge
	int *first_node = &(index->nodes[nodeId].index_out);
	int *last_node = &(index->nodes[nodeId].out_last_node);
	int *last_record = &(index->nodes[nodeId].out_last_record);
	index->nodes[nodeId].size_out++;
	insertEdge(first_node, last_node, last_record, index->out_buffer, targetId, 0);
	HT_Insert(index->nodes[nodeId].neighbors, targetId);
	resizeHash(index, nodeId);

	return SUCCESS;
}


OK_SUCCESS insertNode(NodeIndexPtr index, uint32_t nodeId, uint32_t targetId, uint32_t version) {
	int i = 0;
	uint32_t new_size = index->size;

	// If one node does not fit on table double the size until it fits
	while (nodeId >= new_size || targetId >= new_size) {
		new_size = new_size << 1;
	}
	if (new_size > index->size) {
		IndexNode* new_nodes = NULL;
		// Double the index's size
		new_nodes = c_realloc(index->nodes, new_size * sizeof(IndexNode));

		for (i = index->size; i < new_size; i++) {
			new_nodes[i].index_in = EMPTY;
			new_nodes[i].index_out = EMPTY;
			new_nodes[i].in_last_node = EMPTY;
			new_nodes[i].in_last_record = 0;
			new_nodes[i].out_last_node = EMPTY;
			new_nodes[i].out_last_record = 0;
			new_nodes[i].size_out = 0;
			new_nodes[i].size_in = 0;
			new_nodes[i].neighbors = NULL;//HT_Construct(HASH_SIZE);
		}
		index->size = new_size;
		index->nodes = new_nodes;
	}

	if (index->nodes[nodeId].neighbors == NULL) {
		index->nodes[nodeId].neighbors = HT_Construct(HASH_SIZE);
	}

	int max_node = max(nodeId, targetId);
	if( max_node > (index->node_number - 1))
		index->node_number = max_node + 1;

	// Insert outgoing edge
	// Check edge existence
	if (HT_Search(index->nodes[nodeId].neighbors, targetId) != FAILED)
		return EDGE_EXISTS;

	int *first_node = &(index->nodes[nodeId].index_out);
	int *last_node = &(index->nodes[nodeId].out_last_node);
	int *last_record = &(index->nodes[nodeId].out_last_record);
	index->nodes[nodeId].size_out++;
	insertEdge(first_node, last_node, last_record, index->out_buffer, targetId, version);
	HT_Insert(index->nodes[nodeId].neighbors, targetId);

	// Insert incoming edge
	first_node = &(index->nodes[targetId].index_in);
	last_node = &(index->nodes[targetId].in_last_node);
	last_record = &(index->nodes[targetId].in_last_record);
	index->nodes[targetId].size_in++;
	insertEdge(first_node, last_node, last_record, index->in_buffer, nodeId, version);

	// If hash table size reaches a limit resize it
	resizeHash(index, nodeId);

	return SUCCESS;
}


void resizeHash(NodeIndexPtr index, int nodeId) {
	int j, old_size, current_ListNode, elements;
	int *last_record, *last_node;
	uint32_t *neighbors;
	NodePtr current_node;

	old_size = HT_getSize(index->nodes[nodeId].neighbors);
	if (old_size >= 20*HT_getb(index->nodes[nodeId].neighbors)) {
		HT_Destroy(&(index->nodes[nodeId].neighbors));
		index->nodes[nodeId].neighbors = HT_Construct(old_size << 1);

		current_ListNode = index->nodes[nodeId].index_out;
		last_node = &(index->nodes[nodeId].out_last_node);
		last_record = &(index->nodes[nodeId].out_last_record);

		while (current_ListNode != -1) {
			current_node = getListNode(index->out_buffer, current_ListNode);
			neighbors = getNeighbors(current_node);
			if (current_ListNode == *last_node)
				elements = *last_record;
			else
				elements = N;
			for (j = 0; j < elements; j++)
				HT_Insert(index->nodes[nodeId].neighbors, neighbors[j]);
			current_ListNode = getNextListNode(current_node);
		}
	}
}


ptr getListHeadOut(NodeIndexPtr index, uint32_t nodeId, int *last_record, int *last_node) {
	*last_record = index->nodes[nodeId].out_last_record;
	*last_node = index->nodes[nodeId].out_last_node;
	return index->nodes[nodeId].index_out;
}


ptr getListHeadIn(NodeIndexPtr index, uint32_t nodeId, int *last_record, int *last_node) {
	*last_record = index->nodes[nodeId].in_last_record;
	*last_node = index->nodes[nodeId].in_last_node;
	return index->nodes[nodeId].index_in;
}


OK_SUCCESS destroyNodeIndex(NodeIndexPtr* index) {
	if (*index != NULL) {
		int i = 0;
		for (i = 0; i < (*index)->node_number; i++) {
			if((*index)->nodes[i].neighbors != NULL)
				HT_Destroy( &((*index)->nodes[i].neighbors) );
		}
		destroyBuffer(&(*index)->out_buffer);
		destroyBuffer(&(*index)->in_buffer);
		free((*index)->nodes);
		free(*index);
		*index = NULL;
		return SUCCESS;
	} else {
		return FAILED;
	}
}


uint32_t getSizeIn(NodeIndexPtr index, uint32_t nodeId) {
	if (nodeId < index->size)
		return index->nodes[nodeId].size_in;
	return nodeId;
}


uint32_t getSizeOut(NodeIndexPtr index, uint32_t nodeId) {
	if (nodeId < index->size)
		return index->nodes[nodeId].size_out;
	return nodeId;
}


NodePtr NodeIndex_getListNodeOut(NodeIndexPtr index, Ptr id) {
	return getListNode(index->out_buffer, id);
}


NodePtr NodeIndex_getListNodeIn(NodeIndexPtr index, Ptr id) {
	return getListNode(index->in_buffer, id);
}


NodeIndexIteratorPtr NodeIndex_initIteratorOut(NodeIndexPtr index, uint32_t node_id) {
	Ptr first_node = index->nodes[node_id].index_out;
	int last_record = index->nodes[node_id].out_last_record;
	return initIterator(index->out_buffer, first_node, last_record);
}


int NodeIndex_nextNeighbor(NodeIndexIteratorPtr *it, uint32_t *element) {
	return nextNeighbor(it, element);
}
