#include "buffer.h"


typedef struct ListNode{
    uint32_t neighbor[N];
    uint32_t edge_property[N];
    Ptr next_ListNode;
} ListNode;


typedef struct Buffer{
    ListNode *nodes;
    int last;
    int size;
} Buffer;


typedef struct Iterator{
    int last_record;
    int current_record;
    int node_elements;
    NodePtr record_node;
    BufferPtr buffer;
} Iterator;



BufferPtr createBuffer(int num_nodes) {
    BufferPtr buff;
    buff = c_malloc(sizeof(Buffer));
    buff->nodes = c_malloc(sizeof(ListNode) * num_nodes);

    buff->last = 0;
    buff->size = num_nodes;
    return buff;
}


Ptr allocNewNode(BufferPtr buff) {
    if(buff->last == buff->size){        //Buffer is full
        //double the Buffer's size
        buff->nodes = c_realloc(buff->nodes, (buff->size << 1) * sizeof(ListNode));
        buff->size = buff->size << 1;
    }
    buff->nodes[buff->last].next_ListNode = -1;
    //return last free node and update last variable
    return (buff->last)++;
}


int addNeighbor(NodePtr node, int index, uint32_t id, uint32_t version) {
    if(index < N) {
        node->neighbor[index] = id;
        node->edge_property[index] = version;
        return SUCCESS;
    }
    else
        return FAILED;
}


int getNextListNode(NodePtr node) {
    return node->next_ListNode;
}


void setNextListNode(NodePtr node, Ptr next_ListNode) {
    node->next_ListNode = next_ListNode;
}


NodePtr getListNode(BufferPtr buff, Ptr index) {
    return &buff->nodes[index];
}


uint32_t* getNeighbors(NodePtr node) {
    return node->neighbor;
}

uint32_t* getEdgeProperties(NodePtr node) {
    return node->edge_property;
}


OK_SUCCESS destroyBuffer(BufferPtr *buff){
    if(*buff != NULL){
        free((*buff)->nodes);
        free(*buff);
        *buff = NULL;
        return SUCCESS;
    }
    else
        return FAILED;
}


BufferIteratorPtr initIterator(BufferPtr buffer, Ptr first_node, int last_record) {
    if (first_node == EMPTY)    //id node has no edges
        return NULL;
    BufferIteratorPtr it = c_malloc(sizeof(Iterator));

    it->current_record = 0;
    it->buffer = buffer;
    it->last_record = last_record;
    it->record_node = getListNode(buffer, first_node);
    if (getNextListNode(it->record_node) == -1)
        it->node_elements = last_record;
    else
        it->node_elements = N;
    return it;
}


int nextNeighbor(BufferIteratorPtr *it, uint32_t *element) {
    if (*it == NULL)
        return 0;
    else if ((*it)->current_record < (*it)->node_elements) {
        *element = (*it)->record_node->neighbor[(*it)->current_record++];
        return 1;
    }
    else {  //move to the next ListNode
        int next_node = getNextListNode((*it)->record_node);
        if (next_node == -1) {  //no more nodes
            free(*it);
            *it = NULL;
            return 0;
        }
        else {
            (*it)->record_node = getListNode((*it)->buffer, next_node);
            (*it)->current_record = 0;

            if (getNextListNode((*it)->record_node) == -1)
                (*it)->node_elements = (*it)->last_record;
            else    //ListNode is full
                (*it)->node_elements = N;
            *element = (*it)->record_node->neighbor[(*it)->current_record++];
            return 1;
        }
    }
}
