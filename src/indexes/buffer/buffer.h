#ifndef _BUFFER_H
#define _BUFFER_H

#include <stdint.h>
#include "../../malloc/malloc.h"
#include "../../types.h"

#define EMPTY -3
#define N 7

typedef int Ptr;

typedef struct ListNode* NodePtr;
typedef struct Buffer* BufferPtr;
typedef struct Iterator* BufferIteratorPtr;

BufferPtr createBuffer(int num_nodes);

Ptr allocNewNode(BufferPtr buff);

int addNeighbor(NodePtr node, int index, uint32_t id, uint32_t version);

int getNextListNode(NodePtr node);

void setNextListNode(NodePtr node, Ptr next_ListNode);

NodePtr getListNode(BufferPtr buff, Ptr index);

uint32_t* getNeighbors(NodePtr node);

uint32_t* getEdgeProperties(NodePtr node);

OK_SUCCESS destroyBuffer(BufferPtr *buff);

BufferIteratorPtr initIterator(BufferPtr buffer, Ptr first_node, int last_record);

int nextNeighbor(BufferIteratorPtr *it, uint32_t *element);

#endif /* _BUFFER_H */

