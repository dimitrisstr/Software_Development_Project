#ifndef _COMPONENT_H
#define _COMPONENT_H

#include <stdint.h>
#include "../../malloc/malloc.h"
#include "../buffer/buffer.h"
#include "../node_index.h"
#include "../../data_structures/stack.h"

#define UNDEFINED 0
#define DEFINED 3
#define ON_STACK 2
#define INDEX 0
#define LOWLINK 1
#define EST_NODES_PER_COMP 1

typedef struct SCC* SCCPtr;

typedef struct Component* ComponentPtr;

typedef struct ComponentCursor* ComponentCursorPtr;


uint32_t SCC_getComponentsCount(SCCPtr scc);

void SCC_addComponent(SCCPtr scc, uint32_t nodes_count, uint32_t *node_ids);

SCCPtr estimateStronglyConnectedComponents(NodeIndexPtr graph);

uint32_t findNodeStronglyConnectedComponentID(SCCPtr components, uint32_t nodeId);

OK_SUCCESS iterateStronglyConnectedComponentID
        (SCCPtr components, ComponentCursorPtr *cursor);

bool next_StronglyConnectedComponentID(SCCPtr components, ComponentCursorPtr cursor);

ComponentPtr getStronglyConnectedComponent(ComponentCursorPtr cursor);

OK_SUCCESS destroyStronglyConnectedComponents(SCCPtr *components);

uint32_t Component_getID(ComponentPtr component);

uint32_t* Component_getNodes(ComponentPtr component);

uint32_t Component_getCount(ComponentPtr component);

#endif /* _COMPONENT_H */
