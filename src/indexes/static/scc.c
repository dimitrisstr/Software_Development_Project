#include "scc.h"

typedef struct Component {
    uint32_t component_id;          //current component id
    uint32_t included_nodes_count;  //number of nodes in component
    uint32_t *included_node_ids;    //ids of included nodes
} Component;


typedef struct ComponentCursor {
    Component* component_ptr;   // pointer to current’s iteration component
    uint32_t current_index;
    uint32_t component_id;
} ComponentCursor;


typedef struct SCC {
    Component *components;      // Components index - a vector which stores
                                // the components information
    uint32_t components_count_estimation;
    uint32_t components_count;
    uint32_t *id_belongs_to_component;    //inverted index
} SCC;



SCCPtr createSCC(uint32_t nodes_count);;

void strongConnect(SCCPtr scc, NodeIndexPtr graph, Stack s, int *visited_array, int **vertices_info, uint32_t vertex);

void SCC_addComponent(SCCPtr scc, uint32_t nodes_count, uint32_t *node_ids);

int min(int a, int b) {
    return ((a <= b) ? a : b);
}


/*
 * SCC
 */


uint32_t SCC_getComponentsCount(SCCPtr scc) {
    return scc->components_count;
}


void SCC_addComponent(SCCPtr scc, uint32_t nodes_count, uint32_t *node_ids) {
    static uint32_t component_id = 0;

    //if components don't fit on array double the size
    if (component_id == scc->components_count_estimation) {
        Component *double_components;
        double_components = c_realloc(scc->components, (scc->components_count_estimation << 1) * sizeof(Component));

        scc->components = double_components;
        scc->components_count_estimation = scc->components_count_estimation << 1;
    }

    //add component
    scc->components[component_id].component_id = component_id;
    scc->components[component_id].included_nodes_count = nodes_count;
    scc->components[component_id].included_node_ids = node_ids;

    int i;
    for (i = 0; i < nodes_count; i++)
        scc->id_belongs_to_component[node_ids[i]] = component_id;

    scc->components_count++;
    component_id++;
}


SCCPtr createSCC(uint32_t nodes_count) {
    SCCPtr new_scc = c_malloc(sizeof(SCC));
    new_scc->components_count = 0;
    new_scc->id_belongs_to_component = c_malloc(sizeof(uint32_t) * nodes_count);
    uint32_t estimated_components = nodes_count / EST_NODES_PER_COMP + 1;
    new_scc->components = c_malloc(sizeof(Component) * estimated_components);
    new_scc->components_count_estimation = estimated_components;
    return new_scc;
}


int *createVisitedArray(uint32_t size) {
    int *visited_array = c_malloc(sizeof(int) * size);
    int i;
    for (i = 0; i < size; i++)
        visited_array[i] = UNDEFINED;
    return visited_array;
}


SCCPtr estimateStronglyConnectedComponents(NodeIndexPtr graph) {
    SCCPtr new_scc = createSCC(getSize(graph));

    int *visited_array = createVisitedArray(getSize(graph));
    int **vertices_info = c_malloc(sizeof(int *) * 2);
    vertices_info[INDEX] = c_malloc(sizeof(int) * getSize(graph));
    vertices_info[LOWLINK] = c_malloc(sizeof(int) * getSize(graph));

    Stack s;
    Stack_create(&s);
    uint32_t i;
    //for all vertices
    for (i = 0; i < getSize(graph); i++)
        if (visited_array[i] == UNDEFINED)     //vertex is undefined
            strongConnect(new_scc, graph, s, visited_array, vertices_info, i);

    free(visited_array);
    free(vertices_info[0]);
    free(vertices_info[1]);
    free(vertices_info);
    Stack_destroy(&s);
    return new_scc;
}

/*
 * Recursive tarjan algorithm
 *
void connect(SCCPtr scc, NodeIndexPtr graph, Stack s, int *visited_array, int **vertices_info, uint32_t vertex) {
    static int index = 1;
    vertices_info[INDEX][vertex] = index;       //index
    vertices_info[LOWLINK][vertex] = index;     //lowlink
    index++;

    uint32_t *vertex_pointer, neighbor, *w;
    vertex_pointer = c_malloc(sizeof(uint32_t));
    *vertex_pointer = vertex;
    Stack_push(s, (void *)vertex_pointer);

    visited_array[vertex] = ON_STACK;

    NodeIndexIteratorPtr it;
    it = NodeIndex_initIteratorOut(graph, vertex);
    while (NodeIndex_nextNeighbor(&it, &neighbor)) {
        if (visited_array[neighbor] == UNDEFINED) {
            connect(scc, graph, s, visited_array, vertices_info, neighbor);
            vertices_info[LOWLINK][vertex] = min(vertices_info[LOWLINK][vertex], vertices_info[LOWLINK][neighbor]);
        }
        else if (visited_array[neighbor] == ON_STACK) {
            vertices_info[LOWLINK][vertex] = min(vertices_info[LOWLINK][vertex], vertices_info[INDEX][neighbor]);
        }
    }

    if (vertices_info[LOWLINK][vertex] == vertices_info[INDEX][vertex]) {
        int component_size = 10;
        uint32_t *included_nodes = c_malloc(sizeof(uint32_t) * component_size);
        int i = 0;
        do {
            Stack_pop(s, (void **)&w);
            //if array is full, double the size
            if (i == component_size) {
                component_size = component_size << 1;
                included_nodes = c_realloc(included_nodes, sizeof(uint32_t) * component_size);
            }
            included_nodes[i] = *w;
            visited_array[included_nodes[i]] = DEFINED;
            free(w);
        } while (included_nodes[i++] != vertex);
        included_nodes = c_realloc(included_nodes, sizeof(uint32_t) * i);
        SCC_addComponent(scc, i, included_nodes);
    }
}
*/


void strongConnect(SCCPtr scc, NodeIndexPtr graph, Stack s, int *visited_array, int **vertices_info, uint32_t vertex) {
    static int index = 1;
    NodeIndexIteratorPtr it;
    uint32_t neighbor, *w;
    int backtrack = 0;
    Stack recursion_stack, vertices_stack;
    Stack_create(&recursion_stack);
    Stack_create(&vertices_stack);

    uint32_t *v = c_malloc(sizeof(uint32_t));
    *v = vertex;
    Stack_push(s, v);

    while (Stack_size(s) != 0) {
        if (backtrack == 0) {
            it = NodeIndex_initIteratorOut(graph, vertex);
            Stack_push(vertices_stack, v);
            vertices_info[INDEX][vertex] = index;       //index
            vertices_info[LOWLINK][vertex] = index;     //lowlink
            visited_array[vertex] = ON_STACK;           //node is on stack
            index++;
        }
        else {
            Stack_pop(recursion_stack, (void **) &w);
            Stack_pop(recursion_stack, (void **) &it);
            Stack_peek(vertices_stack, (void**) &v);
            vertices_info[LOWLINK][*v] = min(vertices_info[LOWLINK]
                                             [*v], vertices_info[LOWLINK][*w]);
            vertex = *v;
            free(w);
        }
        //Consider successors of vertex
        while (NodeIndex_nextNeighbor(&it, &neighbor)) {
            if (visited_array[neighbor] == UNDEFINED) {
                uint32_t *succ = c_malloc(sizeof(uint32_t));
                *succ = neighbor;
                Stack_push(recursion_stack, it);
                Stack_push(recursion_stack, succ);
                vertex = neighbor;
                v = c_malloc(sizeof(int));
                *v = vertex;
                Stack_push(s, v);
                backtrack = 0;
                break;
            }
            else if (visited_array[neighbor] == ON_STACK)
                //Successor neighbor is in stack s and hence in the current SCC
                vertices_info[LOWLINK][vertex] = min(vertices_info[LOWLINK]
                                                     [vertex], vertices_info[INDEX][neighbor]);
        }
        if (it != NULL)
            continue;

        Stack_pop(vertices_stack, (void**) &v);
        vertex = *v;
        backtrack = 1;
        // If vertex is a root node, pop the s stack and generate an SCC
        if (vertices_info[LOWLINK][vertex] == vertices_info[INDEX][vertex]) {
            int component_size = 10;
            uint32_t *included_nodes = c_malloc(sizeof(uint32_t) * component_size);
            int i = 0;
            do {
                Stack_pop(s, (void **)&w);
                //if array is full, double the size
                if (i == component_size) {
                    component_size = component_size << 1;
                    included_nodes = c_realloc(included_nodes, sizeof(uint32_t) * component_size);
                }
                included_nodes[i] = *w;
                visited_array[included_nodes[i]] = DEFINED;
                free(w);
            } while (included_nodes[i++] != vertex);
            included_nodes = c_realloc(included_nodes, sizeof(uint32_t) * i);
            SCC_addComponent(scc, i, included_nodes);
        }
    }
    Stack_destroy(&recursion_stack);
    Stack_destroy(&vertices_stack);
}


uint32_t findNodeStronglyConnectedComponentID(SCCPtr components, uint32_t nodeId) {
    return components->id_belongs_to_component[nodeId];
}


OK_SUCCESS destroyStronglyConnectedComponents(SCCPtr *components) {
    if (*components != NULL) {
        int i;
        for (i = 0; i < (*components)->components_count; i++)
            free((*components)->components[i].included_node_ids);
        free((*components)->components);
        free((*components)->id_belongs_to_component);
        free(*components);
        *components = NULL;
        return SUCCESS;
    }
    else
        return FAILED;
}


/*
 * Component + ComponentCursor
 */

OK_SUCCESS iterateStronglyConnectedComponentID
        (SCCPtr scc, ComponentCursorPtr *cursor) {
    *cursor = c_malloc(sizeof(ComponentCursor));
    (*cursor)->component_ptr = scc->components;
    (*cursor)->component_ptr--;
    (*cursor)->current_index = 0;
    return 0;
}


bool next_StronglyConnectedComponentID(SCCPtr components, ComponentCursorPtr cursor) {
    /*
     * move the cursor to the next component
     */
    if (cursor->current_index != components->components_count) {
        cursor->component_ptr++;
        cursor->current_index++;
        cursor->component_id = cursor->component_ptr->component_id;
        return true;
    }
    else {
        free(cursor);
        return false;
    }
}


ComponentPtr getStronglyConnectedComponent(ComponentCursorPtr cursor) {
    return cursor->component_ptr;
}


uint32_t Component_getCount(ComponentPtr component) {
    return component->included_nodes_count;
}


uint32_t* Component_getNodes(ComponentPtr component) {
    return component->included_node_ids;
}


uint32_t Component_getID(ComponentPtr component) {
    return component->component_id;
}
