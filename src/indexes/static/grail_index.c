#include "grail_index.h"


typedef struct GrailLabel {
    uint32_t min_rank;
    uint32_t rank;
} GrailLabel;


typedef struct GrailIndex {
    GrailLabel *nodes_labels1;
    GrailLabel *nodes_labels2;
    SCCPtr components;
} GrailIndex;


void visit(GrailLabel *nodes_labels, NodeIndexPtr graph, uint32_t vertex, uint32_t *rank, int *visited_array,
           Stack vertex_stack, Stack iterator_stack);

NodeIndexPtr createHyperGraph(NodeIndexPtr graph, SCCPtr scc);

void hypergraphInsert(SCCPtr scc, NodeIndexPtr graph, NodeIndexPtr hyper_graph, ComponentPtr component);

uint32_t minNeighborRank(GrailLabel *nodes_labels, NodeIndexPtr graph_index, uint32_t vertex);


/*
 * hyper graph
 */

NodeIndexPtr createHyperGraph(NodeIndexPtr graph, SCCPtr scc) {
    uint32_t components_count = SCC_getComponentsCount(scc);
    NodeIndexPtr hyper_graph = createNodeIndex(components_count);

    ComponentCursorPtr cursor;
    ComponentPtr component;
    iterateStronglyConnectedComponentID(scc, &cursor);
    //for each component add edges
    while (next_StronglyConnectedComponentID(scc, cursor)) {
        component = getStronglyConnectedComponent(cursor);
        hypergraphInsert(scc, graph, hyper_graph, component);
    }

    return hyper_graph;
}


void hypergraphInsert(SCCPtr scc, NodeIndexPtr graph, NodeIndexPtr hyper_graph, ComponentPtr component) {
    uint32_t source_id, target_id;
    uint32_t neighbor, node_id;
    int i;

    uint32_t component_count = Component_getCount(component);
    uint32_t *component_nodes = Component_getNodes(component);

    source_id = Component_getID(component);
    for (i = 0; i < component_count; i++) {
        node_id = component_nodes[i];

        NodeIndexIteratorPtr it = NodeIndex_initIteratorOut(graph, node_id);
        while (NodeIndex_nextNeighbor(&it, &neighbor)) {
            target_id = findNodeStronglyConnectedComponentID(scc, neighbor);
            if (target_id != source_id)
                insertOutNode(hyper_graph, source_id, target_id);
        }
    }
}


GrailIndexPtr buildGrailIndex(NodeIndexPtr graph, SCCPtr scc) {
    uint32_t components_count = SCC_getComponentsCount(scc);
    GrailIndexPtr grail_index = c_malloc(sizeof(GrailIndex));
    grail_index->nodes_labels1 = c_malloc(sizeof(GrailLabel) * components_count);
    grail_index->nodes_labels2 = c_malloc(sizeof(GrailLabel) * components_count);
    grail_index->components = scc;

    NodeIndexPtr hyper_graph = createHyperGraph(graph, scc);

    Stack vertex_stack, iterator_stack;
    Stack_create(&vertex_stack);
    Stack_create(&iterator_stack);

    //create and initialize visited array
    int i, *visited_array = c_malloc(sizeof(int) * components_count);
    for (i = 0; i < components_count; i++)
        visited_array[i] = NOT_VISITED;

    //create first index
    uint32_t rank = 1;
    for (i = 0; i < components_count; i++)
        if (visited_array[i] == NOT_VISITED)
            visit(grail_index->nodes_labels1, hyper_graph, i, &rank, visited_array, vertex_stack, iterator_stack);


    for (i = 0; i < components_count; i++)
        visited_array[i] = NOT_VISITED;

    //create second index
    rank = 1;
    for (i = components_count - 1; i >= 0; i--)
        if (visited_array[i] == NOT_VISITED)
            visit(grail_index->nodes_labels2, hyper_graph, i, &rank, visited_array, vertex_stack, iterator_stack);

    Stack_destroy(&vertex_stack);
    Stack_destroy(&iterator_stack);

    free(visited_array);
    destroyNodeIndex(&hyper_graph);

    return grail_index;
}


uint32_t minNeighborRank(GrailLabel *nodes_labels, NodeIndexPtr graph_index, uint32_t vertex) {
    uint32_t max_value = (uint32_t)getSize(graph_index) + 3, min_value = max_value;
    uint32_t neighbor, value;

    NodeIndexIteratorPtr it = NodeIndex_initIteratorOut(graph_index, vertex);
    while (NodeIndex_nextNeighbor(&it, &neighbor)) {
        value = nodes_labels[neighbor].min_rank;
        if (min_value > value)
            min_value = value;
    }
    return min_value;
}

void visit(GrailLabel *nodes_labels, NodeIndexPtr graph, uint32_t vertex, uint32_t *rank, int *visited_array,
           Stack vertex_stack, Stack iterator_stack) {
    uint32_t min_rank, max_value = (uint32_t) getSize(graph) + 3;
    int backtrack = 0;
    NodeIndexIteratorPtr it;
    uint32_t neighbor, *vertex_pointer;

    vertex_pointer = c_malloc(sizeof(uint32_t));
    *vertex_pointer = vertex;
    Stack_push(vertex_stack, vertex_pointer);

    while (Stack_size(vertex_stack) != 0) {
        if (backtrack == 0) {
            it = NodeIndex_initIteratorOut(graph, *vertex_pointer);
            Stack_push(iterator_stack, it);
        }
        else {
            Stack_peek(iterator_stack, (void **)&it);
            Stack_peek(vertex_stack, (void **)&vertex_pointer);
        }


        if (visited_array[*vertex_pointer] == VISITED && backtrack == 0) {
            backtrack = 1;
            Stack_pop(iterator_stack, (void **)&it);
            Stack_pop(vertex_stack, (void **)&vertex_pointer);
            free(it);
            free(vertex_pointer);
            continue;
        }

        visited_array[*vertex_pointer] = VISITED;
        while (NodeIndex_nextNeighbor(&it, &neighbor)) {
            vertex_pointer = c_malloc(sizeof(uint32_t));
            *vertex_pointer = neighbor;
            Stack_push(vertex_stack, vertex_pointer);
            backtrack = 0;
            break;
        }

        if (it == NULL) {
            backtrack = 1;
            nodes_labels[*vertex_pointer].rank = *rank;
            min_rank = minNeighborRank(nodes_labels, graph, *vertex_pointer);
            if (min_rank == max_value)
                nodes_labels[*vertex_pointer].min_rank = *rank;
            else
                nodes_labels[*vertex_pointer].min_rank = min_rank;
            Stack_pop(iterator_stack, (void **)&it);
            Stack_pop(vertex_stack, (void **)&vertex_pointer);
            free(vertex_pointer);
            (*rank)++;
        }
    }
}


GRAIL_ANSWER isReachableGrailIndex(GrailIndexPtr grail_index, uint32_t source_node,
                                   uint32_t target_node) {
    uint32_t source_component = findNodeStronglyConnectedComponentID(grail_index->components, source_node);
    uint32_t target_component = findNodeStronglyConnectedComponentID(grail_index->components, target_node);

    if (source_component == target_component)
        return YES;

    uint32_t source_min_rank = grail_index->nodes_labels1[source_component].min_rank;
    uint32_t source_rank = grail_index->nodes_labels1[source_component].rank;
    uint32_t target_min_rank = grail_index->nodes_labels1[target_component].min_rank;
    uint32_t target_rank = grail_index->nodes_labels1[target_component].rank;

    if (target_rank <= source_rank && target_min_rank >= source_min_rank) {
        source_min_rank = grail_index->nodes_labels2[source_component].min_rank;
        source_rank = grail_index->nodes_labels2[source_component].rank;
        target_min_rank = grail_index->nodes_labels2[target_component].min_rank;
        target_rank = grail_index->nodes_labels2[target_component].rank;
        if (target_rank <= source_rank && target_min_rank >= source_min_rank)
            return MAYBE;
        else
            return NO;
    }
    else
        return NO;
}


OK_SUCCESS destroyGrailIndex(GrailIndexPtr *index) {
    if (*index != NULL) {
        free((*index)->nodes_labels1);
        free((*index)->nodes_labels2);
        free(*index);
        *index = NULL;
        return SUCCESS;
    }
    else
        return FAILED;
}

/*
 * Recursive buildGrailIndex
 *
GrailIndexPtr buildGrailIndex(NodeIndexPtr graph, SCCPtr scc) {
    int components_count = SCC_getComponentsCount(scc);
    GrailIndexPtr grail_index = malloc(sizeof(GrailIndex));
    grail_index->nodes_labels = malloc(sizeof(GrailLabel) * components_count);

    //create and initialize visited array
    int i, *visited_array = malloc(sizeof(int) * components_count);
    for (i = 0; i < components_count; i++)
        visited_array[i] = 0;

    //create hyper-graph
    NodeIndexPtr hyper_graph = createHyperGraph(graph, scc);

    //create grail index
    for (i = 0; i < components_count; i++)
        if (visited_array[i] == NOT_VISITED)
            visit(grail_index, hyper_graph, i, visited_array);
    grail_index->size = components_count;

    destroyNodeIndex(&hyper_graph);
    free(visited_array);

    return grail_index;
}


void visit(GrailIndexPtr grail_index, NodeIndexPtr graph, uint32_t vertex, int *visited_array) {
    static uint32_t rank = 1;
    int max_value = (uint32_t)getSize(graph) + 3;
    uint32_t min_rank = max_value;  //set min_ran to MAX VALUE
    uint32_t neighbor;

    if (visited_array[vertex] == VISITED)
        return;

    visited_array[vertex] = VISITED;
    NodeIndexIteratorPtr it = NodeIndex_initIteratorOut(graph, vertex);
    while (NodeIndex_nextNeighbor(&it, &neighbor)) {

        visit(grail_index, graph, neighbor, visited_array);
        if (min_rank > grail_index->nodes_labels[neighbor].min_rank)
            min_rank = grail_index->nodes_labels[neighbor].min_rank;
    }

    grail_index->nodes_labels[vertex].rank = rank;
    if (min_rank == max_value)  //node has no neighbors
        grail_index->nodes_labels[vertex].min_rank = rank;
    else
        grail_index->nodes_labels[vertex].min_rank = min_rank;
    rank++;
}
*/
