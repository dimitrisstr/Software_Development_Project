#ifndef _GRAIL_INDEX_H
#define _GRAIL_INDEX_H

#include <pthread.h>
#include <unistd.h>
#include "scc.h"
#include "../../malloc/malloc.h"
#include "../node_index.h"
#include "../../data_structures/stack.h"

#define NOT_VISITED 0
#define VISITED 1

typedef enum GRAIL_ANSWER {NO, MAYBE, YES} GRAIL_ANSWER;

typedef struct GrailIndex* GrailIndexPtr;


GrailIndexPtr buildGrailIndex(NodeIndexPtr graph, SCCPtr scc);

GRAIL_ANSWER isReachableGrailIndex(GrailIndexPtr grail_index, uint32_t source_node,
                                   uint32_t target_node);

OK_SUCCESS destroyGrailIndex(GrailIndexPtr *index);

#endif /* _GRAIL_INDEX_h */
