#include "job.h"


typedef struct Job {
	uint32_t qid;
	uint32_t version;
	uint32_t source;
	uint32_t target;
	JobFunction job_function;
} Job;



JobPtr createJob(uint32_t version, uint32_t source, uint32_t target,
				 JobFunction job_function) {
	JobPtr job = c_malloc(sizeof(Job));

	job->version = version;
	job->source = source;
	job->target = target;
	job->job_function = job_function;

	return job;
}

void setQid(JobPtr job, uint32_t id) {
	job->qid = id;
}

JobFunction getFunction(JobPtr job) {
	return job->job_function;
}

uint32_t getQid(JobPtr job) {
	return job->qid;
}


uint32_t getVersion(JobPtr job) {
	return job->version;
}


uint32_t getSource(JobPtr job) {
	return job->source;
}


uint32_t getTarget(JobPtr job) {
	return job->target;
}


void destroyJob(JobPtr *job) {
	free(*job);
	*job = NULL;
}
