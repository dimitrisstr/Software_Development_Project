#include "job_scheduler.h"


typedef struct JobScheduler {
    NodeIndexPtr graph;         //pointer to existing graph index
    GrailIndexPtr grail_index;  //pointer to existing grail index
    SCCPtr strongly_connected_components;   //pointer to existing scc
    CCPtr weakly_connected_components;	//pointer to existing cc

    bool execute_queries;       //start or stop executing jobs
    uint32_t threads_count;          //number of threads
    int *paths_array;           //stores the paths of all queries
    uint32_t paths_array_size;

    uint32_t jobs_finished_count;
    uint32_t jobs_count;
    Queue jobs_queue;
    pthread_t *tids;            //array of threads

    pthread_mutex_t var_mutex;
    pthread_mutex_t queue_mutex;    //mutex for queue synchronization
    pthread_cond_t cond_execute;    //worker threads wait for jobs execution
    pthread_cond_t cond_master;     //wait for worker threads to finish
} JobScheduler;



void thread_perror_exit(char *message, int error) {
    fprintf(stderr, "%s", message);
    strerror(error);
    exit(EXIT_FAILURE);
}


int *rebuildVisitedArray(int *visited_array, uint32_t old_size, uint32_t new_size) {
    visited_array = c_realloc(visited_array, sizeof(int) * new_size);
    int i;
    for (i = old_size; i < new_size; i++)
        visited_array[i] = 0;
    return visited_array;
}


JobSchedulerPtr initializeScheduler(uint32_t threads_count, GrailIndexPtr grail_index, NodeIndexPtr graph,
                                    SCCPtr strongly_connected_components,
                                    CCPtr weakly_connected_components) {
    JobSchedulerPtr new_scheduler = c_malloc(sizeof(JobScheduler));

    new_scheduler->grail_index = grail_index;
    new_scheduler->graph = graph;
    new_scheduler->strongly_connected_components = strongly_connected_components;
    new_scheduler->weakly_connected_components = weakly_connected_components;

    new_scheduler->execute_queries = false;
    new_scheduler->jobs_finished_count = 0;
    new_scheduler->jobs_count = 0;
    new_scheduler->paths_array_size = RESULTS_ARRAY_SIZE;
    new_scheduler->threads_count = threads_count;
    new_scheduler->paths_array = c_malloc(new_scheduler->paths_array_size * (sizeof(int)));

    Queue_create(&new_scheduler->jobs_queue);
    pthread_mutex_init(&new_scheduler->var_mutex, NULL);
    pthread_mutex_init(&new_scheduler->queue_mutex, NULL);
    pthread_cond_init(&new_scheduler->cond_execute, NULL);
    pthread_cond_init(&new_scheduler->cond_master, NULL);

    //create threads
    new_scheduler->tids = c_malloc(sizeof(pthread_t) * threads_count);
    int i, err;
    for(i = 0; i < threads_count; i++)
        if((err = pthread_create(&new_scheduler->tids[i], NULL, workerThread, new_scheduler)))
            thread_perror_exit("pthread_create", err);

    return new_scheduler;
}


void* workerThread(void *sch) {
    JobSchedulerPtr scheduler = sch;
    JobPtr new_job;
    JobFunction function;

    int i, path_length, bfs_id = 1;
    uint32_t source, target, qid, version = 0;;
    int graph_size = getSize(scheduler->graph), new_graph_size = 0;

    //create and initialize visited array
    int *visited_array = c_malloc(sizeof(int) * graph_size);
    for (i = 0; i < graph_size; i++)
        visited_array[i] = 0;

    while (1)
    {
        pthread_mutex_lock(&scheduler->queue_mutex);
        while (Queue_size(scheduler->jobs_queue) == 0 || scheduler->execute_queries == false)
            pthread_cond_wait(&scheduler->cond_execute, &scheduler->queue_mutex);
        Queue_pop(scheduler->jobs_queue, (void **)&new_job);
        pthread_mutex_unlock(&scheduler->queue_mutex);

        function = getFunction(new_job);
        source = getSource(new_job);
        target = getTarget(new_job);
        qid = getQid(new_job);
        version = getVersion(new_job);
        destroyJob(&new_job);

        if (function == job_static_query) {         //static graph
            GRAIL_ANSWER query = isReachableGrailIndex(scheduler->grail_index, source, target);
            if (query == NO)
                path_length = -1;                
            else if (query == YES)                
                path_length = estimateShortestPathStronglyConnectedComponents(scheduler->strongly_connected_components,
                                                                              scheduler->graph, source, target,
                                                                              visited_array, &bfs_id);
            else
                path_length = bidirectionalBFS(scheduler->graph, scheduler->weakly_connected_components,
                                               scheduler->grail_index, source, target, 0, visited_array, &bfs_id,
                                               job_static_query);

            scheduler->paths_array[qid] = path_length;
        }
        else if (function == job_dynamic_query) {   //dynamic graph
            new_graph_size = getSize(scheduler->graph);
            if (new_graph_size > graph_size) {
                visited_array = rebuildVisitedArray(visited_array, graph_size, new_graph_size);
                graph_size = new_graph_size;
            }
            path_length = bidirectionalBFS(scheduler->graph, scheduler->weakly_connected_components,
                                           scheduler->grail_index, source, target, version, visited_array, &bfs_id,
                                           job_dynamic_query);
            scheduler->paths_array[qid] = path_length;
        }
        else if (function == job_exit) {            //terminate thread
            free(visited_array);
            pthread_exit(NULL);
        }

        pthread_mutex_lock(&scheduler->var_mutex);
        scheduler->jobs_finished_count++;
        if (scheduler->jobs_finished_count == scheduler->jobs_count)
            pthread_cond_signal(&scheduler->cond_master);
        pthread_mutex_unlock(&scheduler->var_mutex);
    }
}


void submitJob(JobSchedulerPtr sch, JobPtr job) {
    setQid(job, sch->jobs_count++);
    Queue_push(sch->jobs_queue, job);
    if (sch->jobs_count > sch->paths_array_size) {
        int *array = c_realloc(sch->paths_array, sch->paths_array_size * 2 * sizeof(int));
        sch->paths_array = array;
        sch->paths_array_size *= 2;
    }
}


void executeAllJobs(JobSchedulerPtr sch) {
    sch->execute_queries = true;
    pthread_cond_broadcast(&sch->cond_execute);
}


void waitAllTasksToFinish(JobSchedulerPtr sch) {
    pthread_mutex_lock(&sch->var_mutex);
    while (sch->jobs_finished_count != sch->jobs_count)
        pthread_cond_wait(&sch->cond_master, &sch->var_mutex);
    pthread_mutex_unlock(&sch->var_mutex);
}


void printAllJobs(JobSchedulerPtr sch) {
    int i;
    for (i = 0; i < sch->jobs_count; i++)
        printf("%d\n", sch->paths_array[i]);
}


void resetScheduler(JobSchedulerPtr sch) {
    sch->jobs_finished_count = 0;
    sch->jobs_count = 0;
    sch->execute_queries = false;
}


OK_SUCCESS destroyScheduler(JobSchedulerPtr *sch) {
    //terminate all threads
    //push a terminate job for each thread
    JobPtr terminate_job;
    int i;
    for (i = 0; i < (*sch)->threads_count; i++) {
        terminate_job = createJob(0, 0, 0, job_exit);
        Queue_push((*sch)->jobs_queue, terminate_job);
    }

    (*sch)->execute_queries = true;
    pthread_cond_broadcast(&(*sch)->cond_execute);

    for (i = 0; i < (*sch)->threads_count; i++)
        pthread_join((*sch)->tids[i], NULL);

    //free resources
    free((*sch)->tids);
    free((*sch)->paths_array);
    pthread_mutex_destroy(&(*sch)->queue_mutex);
    pthread_mutex_destroy(&(*sch)->var_mutex);
    pthread_cond_destroy(&(*sch)->cond_execute);
    pthread_cond_destroy(&(*sch)->cond_master);
    Queue_destroy(&(*sch)->jobs_queue);
    free(*sch);
    *sch = NULL;

    return SUCCESS;
}
