#ifndef _JOB_SCHEDULER_H
#define _JOB_SCHEDULER_H

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include "../malloc/malloc.h"
#include "../data_structures/queue.h"
#include "job.h"
#include "../types.h"
#include "../bfs/bfs.h"
#include "../indexes/static/grail_index.h"
#include "../indexes/node_index.h"
#include "../indexes/static/scc.h"

#define RESULTS_ARRAY_SIZE 256

typedef struct JobScheduler* JobSchedulerPtr;

JobSchedulerPtr initializeScheduler(uint32_t threads_count, GrailIndexPtr grail_index, NodeIndexPtr graph,
									SCCPtr strongly_connected_components,
									CCPtr weakly_connected_components);

void* workerThread(void *sch);

void submitJob(JobSchedulerPtr sch, JobPtr job);

void executeAllJobs(JobSchedulerPtr sch);

void waitAllTasksToFinish(JobSchedulerPtr sch);

void printAllJobs(JobSchedulerPtr sch);

void resetScheduler(JobSchedulerPtr sch);

OK_SUCCESS destroyScheduler(JobSchedulerPtr *sch);

#endif /* _JOB_SCHEDULER_H */
