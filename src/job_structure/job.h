#ifndef _JOB_H_
#define _JOB_H_

#include <stdint.h>
#include "../types.h"
#include "../malloc/malloc.h"

typedef struct Job* JobPtr;

JobPtr createJob(uint32_t version, uint32_t source, uint32_t target,
                 JobFunction job_function);

void setQid(JobPtr job, uint32_t id);

uint32_t getQid(JobPtr);

uint32_t getVersion(JobPtr);

JobFunction getFunction(JobPtr job);

uint32_t getSource(JobPtr);

uint32_t getTarget(JobPtr);

void destroyJob(JobPtr*);

#endif /* _JOB_H_ */
