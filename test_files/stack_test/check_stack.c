#include <stdlib.h>
#include <stdint.h>
#include <check.h>
#include "../../src/data_structures/stack.h"


START_TEST(test_stack_create) {
    Stack new_stack;
    Stack_create(&new_stack);
    ck_assert(new_stack != NULL);
    Stack_destroy(&new_stack);
}
END_TEST


START_TEST(test_stack_push_pop_small) {
    uint32_t *element;    
    Stack new_stack;
    Stack_create(&new_stack);

    element = c_malloc(sizeof(uint32_t));
    *element = 5;
    ck_assert_int_eq(Stack_push(new_stack, element), SUCCESS);
    element = c_malloc(sizeof(uint32_t));
    *element = 1;
    ck_assert_int_eq(Stack_push(new_stack, element), SUCCESS);
    element = c_malloc(sizeof(uint32_t));
    *element = 7;
    ck_assert_int_eq(Stack_push(new_stack, element), SUCCESS);
    element = c_malloc(sizeof(uint32_t));
    *element = 3;
    ck_assert_int_eq(Stack_push(new_stack, element), SUCCESS);
    element = c_malloc(sizeof(uint32_t));
    *element = 16;
    ck_assert_int_eq(Stack_push(new_stack, element), SUCCESS);

    ck_assert_int_eq(Stack_pop(new_stack, (void **)&element), SUCCESS);
    ck_assert_int_eq(*element, 16);  
    free(element);
    ck_assert_int_eq(Stack_pop(new_stack, (void **)&element), SUCCESS);
    ck_assert_int_eq(*element, 3);    
    free(element);
    ck_assert_int_eq(Stack_pop(new_stack, (void **)&element), SUCCESS);
    ck_assert_int_eq(*element, 7);    
    free(element);
    ck_assert_int_eq(Stack_pop(new_stack, (void **)&element), SUCCESS);
    ck_assert_int_eq(*element, 1);    
    free(element);
    ck_assert_int_eq(Stack_pop(new_stack, (void **)&element), SUCCESS);
    ck_assert_int_eq(*element, 5);    
    free(element);
    ck_assert_int_eq(Stack_pop(new_stack, (void **)&element), STACK_EMPTY);
    Stack_destroy(&new_stack);
    free(element);
}
END_TEST


START_TEST(test_stack_push_pop_large) {
    uint32_t *element;
    Stack new_stack;
    Stack_create(&new_stack);
    
    int i;
    for (i = 0; i < 2*STACKNODE_SIZE; i++) {
        element = c_malloc(sizeof(uint32_t));
        *element = i;
        ck_assert_int_eq(Stack_push(new_stack, element), SUCCESS);
    }
    
    for (i = 2*STACKNODE_SIZE -1; i > 0; i--) {
        ck_assert_int_eq(Stack_pop(new_stack, (void **)&element), SUCCESS);
        ck_assert_int_eq(*element, i);
        free(element);
    }
    
    for (i = 0; i < 2*STACKNODE_SIZE; i++) {
        element = c_malloc(sizeof(uint32_t));
        *element = i;
        ck_assert_int_eq(Stack_push(new_stack, element), SUCCESS);
    }
    
    for (i = 2*STACKNODE_SIZE -1; i > STACKNODE_SIZE+1; i--) {
        ck_assert_int_eq(Stack_pop(new_stack, (void **)&element), SUCCESS);
        ck_assert_int_eq(*element, i);
        free(element);
    }
    
    
    element = c_malloc(sizeof(uint32_t));
    *element = 122;
    ck_assert_int_eq(Stack_push(new_stack, element), SUCCESS);
    element = c_malloc(sizeof(uint32_t));
    *element = 99;
    ck_assert_int_eq(Stack_push(new_stack, element), SUCCESS);
    element = c_malloc(sizeof(uint32_t));
    *element = 5;
    ck_assert_int_eq(Stack_push(new_stack, element), SUCCESS);
    element = c_malloc(sizeof(uint32_t));
    *element = 10;
    ck_assert_int_eq(Stack_push(new_stack, element), SUCCESS);
    
    ck_assert_int_eq(Stack_pop(new_stack, (void **)&element), SUCCESS);
    ck_assert_int_eq(10, *element);
    free(element);
    ck_assert_int_eq(Stack_pop(new_stack, (void **)&element), SUCCESS);
    ck_assert_int_eq(5, *element);
    free(element);
    ck_assert_int_eq(Stack_pop(new_stack, (void **)&element), SUCCESS);
    ck_assert_int_eq(99, *element);
    free(element);
    ck_assert_int_eq(Stack_pop(new_stack, (void **)&element), SUCCESS);
    ck_assert_int_eq(122, *element);
    free(element);
    
    for (i = STACKNODE_SIZE +1; i > 0; i--) {
        ck_assert_int_eq(Stack_pop(new_stack,(void**)&element), SUCCESS);
        ck_assert_int_eq(*element, i);
        free(element);
    }
    
    
    Stack_destroy(&new_stack);
}
END_TEST


START_TEST(test_stack_size_destroy) {
    uint32_t *element;
    Stack new_stack;
    Stack_create(&new_stack);

    element = c_malloc(sizeof(uint32_t));
    *element = 5;
    ck_assert_int_eq(Stack_push(new_stack, element), SUCCESS);
    element = c_malloc(sizeof(uint32_t));
    *element = 1;
    ck_assert_int_eq(Stack_push(new_stack, element), SUCCESS);    
    
    Stack_pop(new_stack, (void**)&element);
    ck_assert_int_eq(Stack_size(new_stack), 1);
    ck_assert(new_stack != NULL);
    Stack_destroy(&new_stack);
    ck_assert(new_stack == NULL);
}
END_TEST


Suite *stack_suite(void) {
    Suite *s;
    TCase *tc_core;
    s = suite_create("Stack");
    tc_core = tcase_create("Stack");    
    tcase_add_test(tc_core, test_stack_create);
    tcase_add_test(tc_core, test_stack_push_pop_small);
    tcase_add_test(tc_core, test_stack_push_pop_large);
    tcase_add_test(tc_core, test_stack_size_destroy);
    suite_add_tcase(s, tc_core);

    return s;
}


int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = stack_suite();
    sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
