#include <stdlib.h>
#include <check.h>
#include <stdint.h>
#include "../../src/indexes/dynamic/cc_index.h"
#include "../../src/indexes/node_index.h"

START_TEST(test_cc_index_no_version) {
    NodeIndexPtr index = createNodeIndex(128);

    insertNode(index, 0, 1, 0);
    insertNode(index, 1, 2, 0);
    insertNode(index, 2, 0, 0);
    //insertNode(index, 2, 3);
    insertNode(index, 3, 4, 0);
    insertNode(index, 4, 5, 0);
    insertNode(index, 5, 3, 0);
    //insertNode(index, 6, 5);
    insertNode(index, 6, 7, 0);
    insertNode(index, 7, 8, 0);
    insertNode(index, 8, 9, 0);
    insertNode(index, 9, 6, 0);
    insertNode(index, 9, 10, 0);

    CCPtr components;
    components = estimateConnectedComponents(index);

    //check number of components
    int components_count = getComponentsCount(components);
    ck_assert_int_eq(components_count, 3);

    //first cc is 0 1 2
    ck_assert_int_eq(findNodeConnectedComponentID(components, 0, 0), 0);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 1, 0), 0);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 2, 0), 0);

    //second cc is 3 4 5
    ck_assert_int_eq(findNodeConnectedComponentID(components, 3, 0), 1);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 4, 0), 1);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 5, 0), 1);

    //third cc is 6 7 8 9 10
    ck_assert_int_eq(findNodeConnectedComponentID(components, 6, 0), 2);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 7, 0), 2);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 8, 0), 2);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 9, 0), 2);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 10, 0), 2);

    // Update index has not been used
    ck_assert_int_eq(getMetricVal(components), 0);

    // Connect 2 CCs
    insertNode(index, 2, 3, 0);
    insertNewEdge(components, 2, 3, 0);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 1, 0), findNodeConnectedComponentID(components, 4, 0));

    insertNode(index, 6, 5, 0);
    insertNewEdge(components, 6, 5, 0);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 5, 0), findNodeConnectedComponentID(components, 8, 0));
    ck_assert_int_eq(findNodeConnectedComponentID(components, 1, 0), findNodeConnectedComponentID(components, 10, 0));

    ck_assert_int_eq(getMetricVal(components), 4);

    // Add one new node
    insertNode(index, 6, 11, 0);
    insertNewEdge(components, 6, 11, 0);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 6, 0), findNodeConnectedComponentID(components, 11, 0));

    // Add two new nodes
    insertNode(index, 12, 13, 0);
    insertNewEdge(components, 12, 13, 0);

    ck_assert_int_eq(findNodeConnectedComponentID(components, 12, 0), 3);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 12, 0), findNodeConnectedComponentID(components, 13, 0));

    destroyConnectedComponents(components);
    destroyNodeIndex(&index);    
}
END_TEST

START_TEST(test_cc_index_with_version) {
    NodeIndexPtr index = createNodeIndex(128);

    // version 0
    insertNode(index, 0, 0, 0);
    insertNode(index, 0, 2, 0);
    insertNode(index, 1, 3, 0);
    insertNode(index, 1, 4, 0);
    insertNode(index, 3, 4, 0);
    insertNode(index, 4, 1, 0);
    insertNode(index, 5, 6, 0);

    CCPtr components;
    components = estimateConnectedComponents(index);

    //check number of components
    int components_count = getComponentsCount(components);
    ck_assert_int_eq(components_count, 3);

    //first cc is 0 2
    ck_assert_int_eq(findNodeConnectedComponentID(components, 0, 0), 0);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 2, 0), 0);

    //second cc is 1 3 4
    ck_assert_int_eq(findNodeConnectedComponentID(components, 1, 0), 1);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 3, 0), 1);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 4, 0), 1);

    //third cc is 5 6
    ck_assert_int_eq(findNodeConnectedComponentID(components, 5, 0), 2);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 6, 0), 2);

    // Connect 2 CCs
    insertNode(index, 5, 1, 1);
    insertNewEdge(components, 5, 1, 1);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 5, 1), findNodeConnectedComponentID(components, 1, 1));
    ck_assert_int_eq(findNodeConnectedComponentID(components, 5, 0), 2);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 5, 1), 1);

    // Connect 2 CCs
    insertNode(index, 0, 1, 2);
    insertNewEdge(components, 0, 1, 2);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 5, 2), findNodeConnectedComponentID(components, 1, 2));
    ck_assert_int_eq(findNodeConnectedComponentID(components, 1, 2), findNodeConnectedComponentID(components, 0, 2));
    ck_assert_int_eq(findNodeConnectedComponentID(components, 5, 0), 2);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 5, 1), 1);
    ck_assert_int_eq(findNodeConnectedComponentID(components, 0, 0), 0);

    destroyConnectedComponents(components);
    destroyNodeIndex(&index);    
}
END_TEST

Suite *buffer_suite(void) {
    Suite *s;
    TCase *tc_core;
    s = suite_create("CC");
    tc_core = tcase_create("CC");
    tcase_add_test(tc_core, test_cc_index_no_version);
    tcase_add_test(tc_core, test_cc_index_with_version);
    suite_add_tcase(s, tc_core);

    return s;
}



int main(void) {

    int number_failed;
    Suite *s;
    SRunner *sr;

    s = buffer_suite();
    sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
