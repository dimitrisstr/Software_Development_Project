#include <check.h>

#line 1 "NodeIndex_test.check"
#include "../../src/indexes/node_index.h"
#include "../../src/data_structures/hash.h"
#include <stdio.h>

START_TEST(test_node_index_create)
{
	NodeIndexPtr index;
	
	ck_assert_msg( (index = createNodeIndex(128)) != NULL, "construct function failed");
    free(index);

}
END_TEST

START_TEST(test_node_index_insert_get_head)
{
    int last_record, last_node;
    NodeIndexPtr index = createNodeIndex(128);

    ck_assert_msg( insertNode(index, 0, 1, 0) != FAILED, "insert function failed");
    ck_assert_msg( insertNode(index, 1, 2, 0) != FAILED, "insert function failed");
    ck_assert_msg( insertNode(index, 1, 2, 0) == EDGE_EXISTS, "insert duplicate succeeded");
    ck_assert_msg( insertNode(index, 1, 3, 0) != FAILED, "insert function failed");
    ck_assert_msg( insertNode(index,  0, 128, 0) != FAILED, "insert with realloc failed");
    int i = getListHeadOut(index, 1, &last_record, &last_node);
    printf("%d %d %d\n",i, last_record, last_node);
    ck_assert_msg( destroyNodeIndex(&index) != FAILED, "destroy function failed");
}
END_TEST

START_TEST(test_node_index_stress_test)
{
    int i;
    NodeIndexPtr index = createNodeIndex(128);

    for (i = 0; i < 1000000; i++)
    	ck_assert_msg( insertNode(index, i, i+1, 0) != FAILED, "insert function failed");
    ck_assert_msg( destroyNodeIndex(&index) != FAILED, "destroy function failed");
}
END_TEST

START_TEST(test_node_index_destroy)
{
    NodeIndexPtr index = createNodeIndex(128);
    ck_assert_msg( destroyNodeIndex(&index) != FAILED, "destroy function failed");
    
}
END_TEST

int main(void)
{
    Suite *s1 = suite_create("Core");
    TCase *tc1_1 = tcase_create("Core");
    SRunner *sr = srunner_create(s1);
    int nf;

    suite_add_tcase(s1, tc1_1);
    tcase_add_test(tc1_1, test_node_index_create);
    tcase_add_test(tc1_1, test_node_index_insert_get_head);
    tcase_add_test(tc1_1, test_node_index_stress_test);
    tcase_add_test(tc1_1, test_node_index_destroy);

    srunner_run_all(sr, CK_ENV);
    nf = srunner_ntests_failed(sr);
    srunner_free(sr);

    return nf == 0 ? 0 : 1;
}
