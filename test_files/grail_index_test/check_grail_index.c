#include <stdlib.h>
#include <check.h>
#include <stdint.h>
#include "../../src/indexes/static/grail_index.h"
#include "../../src/indexes/static/scc.h"
#include "../../src/indexes/node_index.h"
#include "../../src/malloc/malloc.h"

START_TEST(test_grail_index_graph1) {
    NodeIndexPtr graph_index = createNodeIndex(5);   
    
    insertNode(graph_index, 1, 2, 0);
    insertNode(graph_index, 1, 3, 0);
    insertNode(graph_index, 6, 2, 0);
    insertNode(graph_index, 2, 4, 0);
    insertNode(graph_index, 2, 5, 0);
    insertNode(graph_index, 4, 5, 0);    
    
    SCCPtr scc = estimateStronglyConnectedComponents(graph_index);
    GrailIndexPtr grail_index = buildGrailIndex(graph_index, scc);
    
    GRAIL_ANSWER grail_answer;
    grail_answer = isReachableGrailIndex(grail_index, 1, 3);
    ck_assert_int_eq(grail_answer, MAYBE);

    grail_answer = isReachableGrailIndex(grail_index, 1, 2);
    ck_assert_int_eq(grail_answer, MAYBE);
    
    grail_answer = isReachableGrailIndex(grail_index, 2, 1);
    ck_assert_int_eq(grail_answer, NO);
    
    grail_answer = isReachableGrailIndex(grail_index, 3, 1);
    ck_assert_int_eq(grail_answer, NO);
    
    grail_answer = isReachableGrailIndex(grail_index, 1, 5);
    ck_assert_int_eq(grail_answer, MAYBE);
    
    grail_answer = isReachableGrailIndex(grail_index, 4, 6);
    ck_assert_int_eq(grail_answer, NO);
    
    grail_answer = isReachableGrailIndex(grail_index, 2, 5);
    ck_assert_int_eq(grail_answer, MAYBE);
    
    grail_answer = isReachableGrailIndex(grail_index, 2, 3);
    ck_assert_int_eq(grail_answer, NO);
    
    grail_answer = isReachableGrailIndex(grail_index, 6, 1);
    ck_assert_int_eq(grail_answer, NO);
    
    grail_answer = isReachableGrailIndex(grail_index, 6, 3);
    ck_assert_int_eq(grail_answer, NO);
    
    grail_answer = isReachableGrailIndex(grail_index, 3, 6);
    ck_assert_int_eq(grail_answer, NO);
    
    destroyGrailIndex(&grail_index);
    ck_assert_msg(grail_index == NULL, "Destroy failed!");
    destroyStronglyConnectedComponents(&scc);    
    destroyNodeIndex(&graph_index);
}
END_TEST

START_TEST(test_grail_index_graph2) {
    NodeIndexPtr graph_index = createNodeIndex(10);
    
    insertNode(graph_index, 0, 1, 0);
    insertNode(graph_index, 1, 2, 0);
    insertNode(graph_index, 2, 0, 0);
    insertNode(graph_index, 2, 3, 0);
    insertNode(graph_index, 3, 4, 0);
    insertNode(graph_index, 4, 5, 0);
    insertNode(graph_index, 5, 3, 0);
    insertNode(graph_index, 6, 5, 0);
    insertNode(graph_index, 6, 7, 0);
    insertNode(graph_index, 7, 8, 0);
    insertNode(graph_index, 8, 9, 0);
    insertNode(graph_index, 9, 6, 0);
    insertNode(graph_index, 9, 10, 0);    
    
    SCCPtr scc = estimateStronglyConnectedComponents(graph_index);
    GrailIndexPtr grail_index = buildGrailIndex(graph_index, scc);
    
    //check number of components
    int components_count = SCC_getComponentsCount(scc);
    ck_assert_int_eq(components_count, 4);
    
    GRAIL_ANSWER grail_answer;
    grail_answer = isReachableGrailIndex(grail_index, 3, 7);
    ck_assert_int_eq(grail_answer, NO);
    
    grail_answer = isReachableGrailIndex(grail_index, 1, 3);
    ck_assert_int_eq(grail_answer, MAYBE);
    
    grail_answer = isReachableGrailIndex(grail_index, 1, 5);
    ck_assert_int_eq(grail_answer, MAYBE);
    
    grail_answer = isReachableGrailIndex(grail_index, 6, 3);
    ck_assert_int_eq(grail_answer, MAYBE);
    
    grail_answer = isReachableGrailIndex(grail_index, 1, 2);
    ck_assert_int_eq(grail_answer, YES);
    
    grail_answer = isReachableGrailIndex(grail_index, 6, 8);
    ck_assert_int_eq(grail_answer, YES);
    
    grail_answer = isReachableGrailIndex(grail_index, 3, 4);
    ck_assert_int_eq(grail_answer, YES);
    
    grail_answer = isReachableGrailIndex(grail_index, 10, 6);
    ck_assert_int_eq(grail_answer, NO);
    
    grail_answer = isReachableGrailIndex(grail_index, 2, 9);
    ck_assert_int_eq(grail_answer, NO);
    
    grail_answer = isReachableGrailIndex(grail_index, 0, 10);
    ck_assert_int_eq(grail_answer, NO);
    
    grail_answer = isReachableGrailIndex(grail_index, 1, 3);
    ck_assert_int_eq(grail_answer, MAYBE);
    
    destroyGrailIndex(&grail_index);    
    destroyStronglyConnectedComponents(&scc);
    destroyNodeIndex(&graph_index);
}
END_TEST


Suite *buffer_suite(void) {
    Suite *s;
    TCase *tc_core;
    s = suite_create("Grail Index");
    tc_core = tcase_create("Grail Index");
    tcase_add_test(tc_core, test_grail_index_graph1);   
    tcase_add_test(tc_core, test_grail_index_graph2);
    suite_add_tcase(s, tc_core);

    return s;
}



int main(void) {
    
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = buffer_suite();
    sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;    
}
