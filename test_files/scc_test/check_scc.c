#include <stdlib.h>
#include <check.h>
#include <stdint.h>
#include "../../src/indexes/static/scc.h"
#include "../../src/indexes/node_index.h"
#include "../../src/malloc/malloc.h"

START_TEST(test_scc_graph1) {
    NodeIndexPtr index = createNodeIndex(10);   
    
    insertNode(index, 0, 1, 0);
    insertNode(index, 1, 2, 0);
    insertNode(index, 2, 0, 0);
    insertNode(index, 2, 3, 0);
    insertNode(index, 3, 4, 0);
    insertNode(index, 4, 5, 0);
    insertNode(index, 5, 3, 0);
    insertNode(index, 6, 5, 0);
    insertNode(index, 6, 7, 0);
    insertNode(index, 7, 8, 0);
    insertNode(index, 8, 9, 0);
    insertNode(index, 9, 6, 0);
    insertNode(index, 9, 10, 0);          
    
    SCCPtr new_scc;    
    new_scc = estimateStronglyConnectedComponents(index);
    
    //check number of components
    int components_count = SCC_getComponentsCount(new_scc);
    ck_assert_int_eq(components_count, 4);
    
    uint32_t *nodes;
    ComponentPtr component;    
    ComponentCursorPtr new_cursor;
    iterateStronglyConnectedComponentID(new_scc, &new_cursor);         
    
    //first scc is 5 4 3    
    next_StronglyConnectedComponentID(new_scc, new_cursor);
    component = getStronglyConnectedComponent(new_cursor);
    nodes = Component_getNodes(component);
    ck_assert_int_eq(nodes[0], 5);
    ck_assert_int_eq(nodes[1], 4);
    ck_assert_int_eq(nodes[2], 3);
    
    //second scc is 2 1 0
    next_StronglyConnectedComponentID(new_scc, new_cursor);
    component = getStronglyConnectedComponent(new_cursor);
    nodes = Component_getNodes(component);
    ck_assert_int_eq(nodes[0], 2);
    ck_assert_int_eq(nodes[1], 1);
    ck_assert_int_eq(nodes[2], 0);
    
    //third scc is 10
    next_StronglyConnectedComponentID(new_scc, new_cursor);
    component = getStronglyConnectedComponent(new_cursor);
    nodes = Component_getNodes(component);
    ck_assert_int_eq(nodes[0], 10);    
    
    //fourth scc is 9 8 7 6
    next_StronglyConnectedComponentID(new_scc, new_cursor);
    component = getStronglyConnectedComponent(new_cursor);
    nodes = Component_getNodes(component);
    ck_assert_int_eq(nodes[0], 9);
    ck_assert_int_eq(nodes[1], 8); 
    ck_assert_int_eq(nodes[2], 7); 
    ck_assert_int_eq(nodes[3], 6); 
    
    destroyStronglyConnectedComponents(&new_scc);
    ck_assert_msg(new_scc == NULL, "Destroy failed!");
    destroyNodeIndex(&index);
}
END_TEST

START_TEST(test_scc_graph2) {
    NodeIndexPtr index = createNodeIndex(10);
    
    insertNode(index, 0, 7, 0);
    insertNode(index, 1, 0, 0);
    insertNode(index, 2, 1, 0);
    insertNode(index, 2, 3, 0);
    insertNode(index, 3, 2, 0);
    insertNode(index, 4, 4, 0);
    insertNode(index, 4, 3, 0);
    insertNode(index, 4, 5, 0);
    insertNode(index, 5, 2, 0);
    insertNode(index, 5, 6, 0);
    insertNode(index, 6, 5, 0);
    insertNode(index, 6, 1, 0);
    insertNode(index, 6, 7, 0);
    insertNode(index, 7, 1, 0);
    
    SCCPtr new_scc;    
    new_scc = estimateStronglyConnectedComponents(index);
    
    //check number of components
    int components_count = SCC_getComponentsCount(new_scc);
    ck_assert_int_eq(components_count, 4);
    
    uint32_t *nodes;
    ComponentPtr component;    
    ComponentCursorPtr new_cursor;
    iterateStronglyConnectedComponentID(new_scc, &new_cursor);    
    
    //first scc is 1 7 0    
    next_StronglyConnectedComponentID(new_scc, new_cursor);
    component = getStronglyConnectedComponent(new_cursor);
    nodes = Component_getNodes(component);
    ck_assert_int_eq(nodes[0], 1);
    ck_assert_int_eq(nodes[1], 7);
    ck_assert_int_eq(nodes[2], 0);
    
    //second scc is 3 2
    next_StronglyConnectedComponentID(new_scc, new_cursor);
    component = getStronglyConnectedComponent(new_cursor);
    nodes = Component_getNodes(component);
    ck_assert_int_eq(nodes[0], 3);
    ck_assert_int_eq(nodes[1], 2);
    
    //third scc is 6 5
    next_StronglyConnectedComponentID(new_scc, new_cursor);
    component = getStronglyConnectedComponent(new_cursor);
    nodes = Component_getNodes(component);
    ck_assert_int_eq(nodes[0], 6);
    ck_assert_int_eq(nodes[1], 5);
    
    //fourth scc is 4
    next_StronglyConnectedComponentID(new_scc, new_cursor);
    component = getStronglyConnectedComponent(new_cursor);
    nodes = Component_getNodes(component);
    ck_assert_int_eq(nodes[0], 4);      
    
    destroyStronglyConnectedComponents(&new_scc);    
    ck_assert_msg(new_scc == NULL, "Destroy failed!");
    destroyNodeIndex(&index);
}
END_TEST


START_TEST(test_scc_graph3) {
    NodeIndexPtr index = createNodeIndex(10);
    
    insertNode(index, 0, 1, 0);
    insertNode(index, 0, 2, 0);
    insertNode(index, 1, 3, 0);
    insertNode(index, 1, 4, 0);
    insertNode(index, 3, 4, 0);
    insertNode(index, 4, 1, 0);
    insertNode(index, 5, 1, 0);    
    
    SCCPtr new_scc;    
    new_scc = estimateStronglyConnectedComponents(index);
    
    //check number of components
    int components_count = SCC_getComponentsCount(new_scc);
    ck_assert_int_eq(components_count, 4);
    
    uint32_t *nodes;
    ComponentPtr component;    
    ComponentCursorPtr new_cursor;
    iterateStronglyConnectedComponentID(new_scc, &new_cursor);    
    
    //first scc is 4 3 1 
    next_StronglyConnectedComponentID(new_scc, new_cursor);
    component = getStronglyConnectedComponent(new_cursor);
    nodes = Component_getNodes(component);
    ck_assert_int_eq(nodes[0], 4);
    ck_assert_int_eq(nodes[1], 3);
    ck_assert_int_eq(nodes[2], 1);
    
    //second scc is 2
    next_StronglyConnectedComponentID(new_scc, new_cursor);
    component = getStronglyConnectedComponent(new_cursor);
    nodes = Component_getNodes(component);
    ck_assert_int_eq(nodes[0], 2);    
    
    //third scc is 0
    next_StronglyConnectedComponentID(new_scc, new_cursor);
    component = getStronglyConnectedComponent(new_cursor);
    nodes = Component_getNodes(component);
    ck_assert_int_eq(nodes[0], 0);    
    
    //fourth scc is 5
    next_StronglyConnectedComponentID(new_scc, new_cursor);
    component = getStronglyConnectedComponent(new_cursor);
    nodes = Component_getNodes(component);
    ck_assert_int_eq(nodes[0], 5);      
    
    destroyStronglyConnectedComponents(&new_scc);
    ck_assert_msg(new_scc == NULL, "Destroy failed!");
    destroyNodeIndex(&index);
}
END_TEST

START_TEST(test_scc_graph4) {
    NodeIndexPtr index = createNodeIndex(10);
    
    insertNode(index, 0, 1, 0);
    insertNode(index, 1, 2, 0);
    insertNode(index, 2, 0, 0);
    insertNode(index, 2, 3, 0);
    insertNode(index, 3, 4, 0);
    insertNode(index, 4, 5, 0);
    insertNode(index, 5, 3, 0);
    insertNode(index, 6, 5, 0);
    insertNode(index, 6, 7, 0);
    insertNode(index, 7, 8, 0);
    insertNode(index, 8, 9, 0);
    insertNode(index, 9, 6, 0);
    insertNode(index, 9, 10, 0);    
    insertNode(index, 4, 0, 0);   
    insertNode(index, 9, 4, 0);
    insertNode(index, 7, 4, 0);
    insertNode(index, 4, 9, 0);
    
    SCCPtr new_scc;    
    new_scc = estimateStronglyConnectedComponents(index);
    
    //check number of components
    int components_count = SCC_getComponentsCount(new_scc);
    ck_assert_int_eq(components_count, 2);
    
    uint32_t *nodes;
    ComponentPtr component;    
    ComponentCursorPtr new_cursor;
    iterateStronglyConnectedComponentID(new_scc, &new_cursor);     
   
    //first scc is 10
    next_StronglyConnectedComponentID(new_scc, new_cursor);
    component = getStronglyConnectedComponent(new_cursor);
    nodes = Component_getNodes(component);
    ck_assert_int_eq(nodes[0], 10);   
    
    //second scc is 8 7 6 9 5 4 3 2 1 0
    next_StronglyConnectedComponentID(new_scc, new_cursor);
    component = getStronglyConnectedComponent(new_cursor);
    nodes = Component_getNodes(component);
    ck_assert_int_eq(nodes[0], 8);
    ck_assert_int_eq(nodes[1], 7); 
    ck_assert_int_eq(nodes[2], 6);
    ck_assert_int_eq(nodes[3], 9);
    ck_assert_int_eq(nodes[4], 5);
    ck_assert_int_eq(nodes[5], 4);
    ck_assert_int_eq(nodes[6], 3);
    ck_assert_int_eq(nodes[7], 2);
    ck_assert_int_eq(nodes[8], 1);
    ck_assert_int_eq(nodes[9], 0);     
    
    destroyStronglyConnectedComponents(&new_scc);
    ck_assert_msg(new_scc == NULL, "Destroy failed!");
    destroyNodeIndex(&index);
}
END_TEST

Suite *buffer_suite(void) {
    Suite *s;
    TCase *tc_core;
    s = suite_create("SCC");
    tc_core = tcase_create("SCC");
    tcase_add_test(tc_core, test_scc_graph1);   
    tcase_add_test(tc_core, test_scc_graph2);
    tcase_add_test(tc_core, test_scc_graph3);
    tcase_add_test(tc_core, test_scc_graph4);
    suite_add_tcase(s, tc_core);

    return s;
}



int main(void) {
    
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = buffer_suite();
    sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;    
}
