#include <stdlib.h>
#include <stdint.h>
#include <check.h>
#include "../../src/data_structures/queue.h"
#include "../../src/malloc/malloc.h"

START_TEST(test_queue_create) {
    Queue new_queue;
    Queue_create(&new_queue);
    ck_assert(new_queue != NULL);
    free(new_queue);
}
END_TEST

START_TEST(test_queue_push_pop) {
    uint32_t *element;
    Queue new_queue;
    Queue_create(&new_queue);
    
    uint32_t *vertex = c_malloc(sizeof(uint32_t));
    *vertex = 5;
    ck_assert_int_eq(Queue_push(new_queue, vertex), SUCCESS);
    vertex = c_malloc(sizeof(uint32_t));
    *vertex = 1;
    ck_assert_int_eq(Queue_push(new_queue, vertex), SUCCESS);
    vertex = c_malloc(sizeof(uint32_t));
    *vertex = 7;
    ck_assert_int_eq(Queue_push(new_queue, vertex), SUCCESS);
    vertex = c_malloc(sizeof(uint32_t));
    *vertex = 3;
    ck_assert_int_eq(Queue_push(new_queue, vertex), SUCCESS);
    vertex = c_malloc(sizeof(uint32_t));
    *vertex = 16;
    ck_assert_int_eq(Queue_push(new_queue, vertex), SUCCESS);

    ck_assert_int_eq(Queue_pop(new_queue, (void **) &element), SUCCESS);
    ck_assert_int_eq(*element, 5);
    free(element);
    ck_assert_int_eq(Queue_pop(new_queue, (void **) &element), SUCCESS);
    ck_assert_int_eq(*element, 1);
    free(element);
    ck_assert_int_eq(Queue_pop(new_queue, (void **) &element), SUCCESS);
    ck_assert_int_eq(*element, 7);
    free(element);
    ck_assert_int_eq(Queue_pop(new_queue, (void **) &element), SUCCESS);
    ck_assert_int_eq(*element, 3);
    free(element);
    ck_assert_int_eq(Queue_pop(new_queue, (void **) &element), SUCCESS);
    ck_assert_int_eq(*element, 16);
    free(element);
    ck_assert_int_eq(Queue_pop(new_queue, (void **) &element), QUEUE_EMPTY);
    
    Queue_destroy(&new_queue);    
}
END_TEST

START_TEST(test_queue_push_pop_peek_large) {
    uint32_t *element;
    Queue new_queue;
    Queue_create(&new_queue);
    
    int i;
    for (i = 0; i < 2*QUEUENODE_SIZE + 15; i++) {
        element = c_malloc(sizeof(uint32_t));
        *element = i;
        ck_assert_int_eq(Queue_push(new_queue, element), SUCCESS);
    }
    
    for (i = 0; i < 2*QUEUENODE_SIZE + 15; i++) {
        ck_assert_int_eq(Queue_peek(new_queue, (void **)&element), SUCCESS);
        ck_assert_int_eq(*element, i);
        ck_assert_int_eq(Queue_pop(new_queue, (void **)&element), SUCCESS);
        ck_assert_int_eq(*element, i);
        free(element);
    }
    
    Queue_destroy(&new_queue);    
}
END_TEST

START_TEST(test_queue_size_destroy) {
    uint32_t *element;
    Queue new_queue;
    Queue_create(&new_queue);

    uint32_t *vertex = c_malloc(sizeof(uint32_t));
    *vertex = 5;
    ck_assert_int_eq(Queue_push(new_queue, vertex), SUCCESS);
    vertex = c_malloc(sizeof(uint32_t));
    *vertex = 1;
    ck_assert_int_eq(Queue_push(new_queue, vertex), SUCCESS);
    
    ck_assert_int_eq(Queue_size(new_queue), 2);
    Queue_pop(new_queue, (void **) &element);
    free(element);
    ck_assert_int_eq(Queue_size(new_queue), 1);
    ck_assert(new_queue != NULL);
    Queue_destroy(&new_queue);
    ck_assert(new_queue == NULL);
}
END_TEST


Suite *queue_suite(void) {
    Suite *s;
    TCase *tc_core;
    s = suite_create("Queue");
    tc_core = tcase_create("Queue");
    tcase_add_test(tc_core, test_queue_create);
    tcase_add_test(tc_core, test_queue_push_pop);
    tcase_add_test(tc_core, test_queue_push_pop_peek_large);
    tcase_add_test(tc_core, test_queue_size_destroy);
    suite_add_tcase(s, tc_core);

    return s;
}


int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = queue_suite();
    sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
