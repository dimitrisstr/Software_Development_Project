#include <stdlib.h>
#include <check.h>
#include "../../src/indexes/buffer/buffer.h"
#include "../../src/indexes/node_index.h"
#include "../../src/indexes/static/grail_index.h"
#include "../../src/indexes/static/scc.h"
#include "../../src/bfs/bfs.h"
#include "../../src/data_structures/queue.h"
#include "../../src/types.h"
#include "../../src/malloc/malloc.h"


START_TEST(test_bfs_static) {
    NodeIndexPtr graph_index = createNodeIndex(10);
    
    insertNode(graph_index, 0, 1, 0);
    insertNode(graph_index, 0, 2, 0);
    insertNode(graph_index, 0, 3, 0);
    insertNode(graph_index, 1, 5, 0);
    insertNode(graph_index, 1, 7, 0);
    insertNode(graph_index, 4, 0, 0);
    insertNode(graph_index, 5, 9, 0);
    insertNode(graph_index, 7, 6, 0);
    insertNode(graph_index, 7, 9, 0);
    insertNode(graph_index, 9, 8, 0);
    
    
    SCCPtr scc = estimateStronglyConnectedComponents(graph_index);
    GrailIndexPtr grail_index = buildGrailIndex(graph_index, scc);
    
    int i, *visited_array = c_malloc(sizeof(int) * 100);
    for (i = 0; i < 100; i++)
        visited_array[i] = 0;
    
    int found, bfs_id = 2;
    //path: 0 -> 9     length: 3
    found = bidirectionalBFS(graph_index, NULL, grail_index, 0, 9, 0, visited_array,
                                                        &bfs_id, job_static_query); 
    ck_assert_msg(found != -1, "path from 0 to 9 not found!");
    ck_assert_msg(found == 3, "wrong path length! 0 -> 9");
    //path: 7 -> 9     length: 1
    found = bidirectionalBFS(graph_index, NULL, grail_index, 7, 9, 0, visited_array,
                                                        &bfs_id, job_static_query); 
    ck_assert_msg(found != -1, "path from 7 to 9 not found!");
    ck_assert_msg(found == 1, "wrong path length! 7 -> 9");
    //path: 9 -> 8     length: 1
    found = bidirectionalBFS(graph_index, NULL, grail_index, 9, 8, 0, visited_array,
                                                        &bfs_id, job_static_query); 
    ck_assert_msg(found != -1, "path from 9 to 8 not found!");
    ck_assert_msg(found == 1, "wrong path length! 9 -> 8");
    //path: 8 -> 8     length: 0
    found = bidirectionalBFS(graph_index, NULL, grail_index, 8, 8, 0, visited_array,
                                                        &bfs_id, job_static_query); 
    ck_assert_msg(found != -1, "path from 8 to 8 not found!");
    ck_assert_msg(found == 0, "wrong path length! 8 -> 8");
    //path: 7 -> 8     length: 2
    found = bidirectionalBFS(graph_index, NULL, grail_index, 7, 8, 0, visited_array,
                                                        &bfs_id, job_static_query); 
    ck_assert_msg(found != -1, "path from 7 to 8 not found!");
    ck_assert_msg(found == 2, "wrong path length! 7 -> 8");
    //path: 1 -> 8     length: 3
    found = bidirectionalBFS(graph_index, NULL, grail_index, 1, 8, 0, visited_array,
                                                        &bfs_id, job_static_query); 
    ck_assert_msg(found != -1, "path from 1 to 8 not found!");
    ck_assert_msg(found == 3, "wrong path length! 1 -> 8");
    
    free(visited_array);
    destroyGrailIndex(&grail_index);    
    destroyStronglyConnectedComponents(&scc);
    destroyNodeIndex(&graph_index);
    
}
END_TEST

START_TEST(test_bfs_dynamic) {
    NodeIndexPtr graph_index = createNodeIndex(10);    
    
    insertNode(graph_index, 0, 1, 0);
    insertNode(graph_index, 0, 2, 0);
    insertNode(graph_index, 0, 3, 0);
    insertNode(graph_index, 1, 5, 0);
    insertNode(graph_index, 1, 7, 0);
    insertNode(graph_index, 4, 0, 0);
    insertNode(graph_index, 5, 9, 0);
    insertNode(graph_index, 7, 6, 0);
    insertNode(graph_index, 7, 9, 0);
    insertNode(graph_index, 9, 8, 0);
    
    CCPtr weak_components = estimateConnectedComponents(graph_index);
    
    int i, *visited_array = c_malloc(sizeof(int) * 100);
    for (i = 0; i < 100; i++)
        visited_array[i] = 0;
    
    int found, bfs_id = 2;
    //path: 0 -> 9     length: 3
    found = bidirectionalBFS(graph_index, weak_components, NULL, 0, 9, 0, visited_array,
                                                        &bfs_id, job_dynamic_query); 
    ck_assert_msg(found != -1, "path from 0 to 9 not found!");
    ck_assert_msg(found == 3, "wrong path length! 0 -> 9");
    //path: 7 -> 9     length: 1
    found = bidirectionalBFS(graph_index, weak_components, NULL, 7, 9, 0, visited_array,
                                                        &bfs_id, job_dynamic_query); 
    ck_assert_msg(found != -1, "path from 7 to 9 not found!");
    ck_assert_msg(found == 1, "wrong path length! 7 -> 9");
    //path: 9 -> 8     length: 1
    found = bidirectionalBFS(graph_index, weak_components, NULL, 9, 8, 0, visited_array,
                                                        &bfs_id, job_dynamic_query); 
    ck_assert_msg(found != -1, "path from 9 to 8 not found!");
    ck_assert_msg(found == 1, "wrong path length! 9 -> 8");
    //path: 8 -> 8     length: 0
    found = bidirectionalBFS(graph_index, weak_components, NULL, 8, 8, 0, visited_array,
                                                        &bfs_id, job_dynamic_query); 
    ck_assert_msg(found != -1, "path from 8 to 8 not found!");
    ck_assert_msg(found == 0, "wrong path length! 8 -> 8");
    //path: 7 -> 8     length: 2
    found = bidirectionalBFS(graph_index, weak_components, NULL, 7, 8, 0, visited_array,
                                                        &bfs_id, job_dynamic_query); 
    ck_assert_msg(found != -1, "path from 7 to 8 not found!");
    ck_assert_msg(found == 2, "wrong path length! 7 -> 8");
    //path: 1 -> 8     length: 3
    found = bidirectionalBFS(graph_index, weak_components, NULL, 1, 8, 0, visited_array,
                                                        &bfs_id, job_dynamic_query); 
    ck_assert_msg(found != -1, "path from 1 to 8 not found!");
    ck_assert_msg(found == 3, "wrong path length! 1 -> 8");
    
    free(visited_array);
    destroyConnectedComponents(weak_components);
    destroyNodeIndex(&graph_index);    
}
END_TEST


Suite *buffer_suite(void) {
    Suite *s;
    TCase *tc_core;
    s = suite_create("BFS");
    tc_core = tcase_create("BFS");
    tcase_add_test(tc_core, test_bfs_static);
    tcase_add_test(tc_core, test_bfs_dynamic);
    suite_add_tcase(s, tc_core);

    return s;
}



int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = buffer_suite();
    sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
