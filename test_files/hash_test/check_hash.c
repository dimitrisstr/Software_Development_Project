#include <check.h>

#line 1 "Hash_test.check"
#include "../../src/data_structures/hash.h"
#include <stdlib.h>

START_TEST(test_hash_create)
{
#line 5
	hashPtr ht;
	ck_assert_msg( (ht = HT_Construct(16)) != NULL, "hash construction failed");
	free(ht);

}
END_TEST

START_TEST(test_hash_insert_search)
{
#line 10
    hashPtr ht = HT_Construct(16);
    ck_assert_msg(HT_Insert(ht, 0) == SUCCESS, "hash insertion failed");
    ck_assert_msg(HT_Insert(ht, 5) == SUCCESS, "hash insertion failed");
    ck_assert_msg(HT_Insert(ht, 18) == SUCCESS, "hash insertion failed");
    ck_assert_msg(HT_Insert(ht, 32) == SUCCESS, "hash insertion failed");
    ck_assert_msg(HT_Insert(ht, 64) == SUCCESS, "hash insertion failed");

    ck_assert_msg(HT_Search(ht, 32) == SUCCESS, "hash search failed");
    ck_assert_msg(HT_Search(ht, 5) == SUCCESS, "hash search failed");
    ck_assert_msg(HT_Search(ht, 0) == SUCCESS, "hash search failed");
    ck_assert_msg(HT_Search(ht, 2) == FAILED, "hash search failed");
    free(ht);

}
END_TEST

START_TEST(test_hash_destroy)
{
#line 24
    hashPtr ht = HT_Construct(16);
    HT_Destroy(&ht);
    ck_assert_msg(ht == NULL, "hash destroy failed");
}
END_TEST

START_TEST(test_hash_stress_test)
{
	int i;
    hashPtr ht = HT_Construct(500000);
    for (i = 0; i < 1000000; i++)
    	ck_assert_msg(HT_Insert(ht, i) == SUCCESS, "hash insertion failed");

    HT_Destroy(&ht);

}
END_TEST

int main(void)
{
    Suite *s1 = suite_create("Core");
    TCase *tc1_1 = tcase_create("Core");
    SRunner *sr = srunner_create(s1);
    int nf;

    suite_add_tcase(s1, tc1_1);
    tcase_add_test(tc1_1, test_hash_create);
    tcase_add_test(tc1_1, test_hash_insert_search);
    tcase_add_test(tc1_1, test_hash_destroy);
    tcase_add_test(tc1_1, test_hash_stress_test);

    srunner_run_all(sr, CK_ENV);
    nf = srunner_ntests_failed(sr);
    srunner_free(sr);

    return nf == 0 ? 0 : 1;
}
