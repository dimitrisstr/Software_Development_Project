#include <stdlib.h>
#include <check.h>
#include <stdint.h>
#include "../../src/indexes/buffer/buffer.h"

START_TEST(test_buffer_create_destroy) {
    BufferPtr new_buffer;

    new_buffer = createBuffer(10);
    ck_assert(new_buffer != NULL);

    int success = destroyBuffer(&new_buffer);
    ck_assert_int_eq(success, SUCCESS);
    ck_assert(new_buffer == NULL);

    success = destroyBuffer(&new_buffer);
    ck_assert_int_eq(success, FAILED);
}
END_TEST

START_TEST(test_buffer_allocNewNode) {
    BufferPtr new_buffer;
    int i, buffer_nodes = 128;
    Ptr node;

    new_buffer = createBuffer(buffer_nodes);
    //allocate all buffer nodes
    for(i = 0; i < buffer_nodes; i++) {
        node = allocNewNode(new_buffer);
        ck_assert_int_eq(node, i);
    }

    NodePtr pointer;

    //doubles the buffer's nodes
    node = allocNewNode(new_buffer);
    ck_assert_int_eq(node, buffer_nodes);

    //buffer's size = 2 * buffer_nodes -1
    pointer = getListNode(new_buffer, buffer_nodes << 1 -1);
    ck_assert(pointer != NULL);

    destroyBuffer(&new_buffer);
}
END_TEST

START_TEST(test_buffer_addNeighbor) {
    BufferPtr new_buffer;

    new_buffer = createBuffer(10);

    //allocate first ListNode
    Ptr index = allocNewNode(new_buffer);
    ck_assert_int_eq(index, 0);

    //get first ListNode
    NodePtr pointer = getListNode(new_buffer, 0);
    ck_assert(pointer != NULL);

    //add N neighbors
    int i, success;
    for(i = 0; i < N; i++) {
        success = addNeighbor(pointer, i, i, 0);
        ck_assert_int_eq(success, SUCCESS);
    }

    //ListNode is full
    success = addNeighbor(pointer, N, 50, 0);
    ck_assert_int_eq(success, FAILED);

    //allocate second ListNode
    index = allocNewNode(new_buffer);
    ck_assert_int_eq(index, 1);

    success = addNeighbor(pointer, index, i, 0);
    ck_assert_int_eq(success, SUCCESS);

    setNextListNode(pointer, index);

    ck_assert_int_eq(1, getNextListNode(pointer));

    destroyBuffer(&new_buffer);
}
END_TEST

START_TEST (test_buffer_getNeighbor) {
    BufferPtr new_buffer;
    new_buffer = createBuffer(10);
    //allocate first ListNode
    Ptr startPointer = allocNewNode(new_buffer);
    //allocate second ListNode
    Ptr endPointer = allocNewNode(new_buffer);

    //get first ListNode
    NodePtr startNode = getListNode(new_buffer, startPointer);
    NodePtr endNode = getListNode(new_buffer, endPointer);


    //add N neighbors
    int i, success;
    for(i = 0; i < N; i++)
        success = addNeighbor(startNode, i, i, 0);

    //point to second node
    setNextListNode(startNode, endPointer);

    for(i = N; i < 2*N; i++)
        success = addNeighbor(endNode, i - N, i, 0);

    //last record of the second block (index)
    int last_record = N -1;

    BufferIteratorPtr it = initIterator(new_buffer, startPointer, last_record);
    ck_assert_msg(it != NULL, "init iterator failed");

    i = 0;
    uint32_t neighbor;
    //get 2*N neighbors
    while (nextNeighbor(&it, &neighbor)) {        
        ck_assert_int_eq(i, neighbor);        
        i++;
    }
    
    ck_assert_int_eq(i, 2*N -1);
    free(it);
    destroyBuffer(&new_buffer);
}
END_TEST

Suite *buffer_suite(void) {
    Suite *s;
    TCase *tc_core;
    s = suite_create("Buffer");
    tc_core = tcase_create("Buffer");
    tcase_add_test(tc_core, test_buffer_create_destroy);
    tcase_add_test(tc_core, test_buffer_allocNewNode);
    tcase_add_test(tc_core, test_buffer_addNeighbor);
    tcase_add_test(tc_core, test_buffer_getNeighbor);
    suite_add_tcase(s, tc_core);

    return s;
}



int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = buffer_suite();
    sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
